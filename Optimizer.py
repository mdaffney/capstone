# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 10:10:21 2023

@author: kents
"""

# Packages
import os
import pandas as pd
import numpy as np
import sqlite3
import math
import datetime

from ortools.linear_solver import pywraplp

# Our project classes
from Model import Model


class Optimizer:
    
    def __init__(self, wager, strategy, model_name, feature_set_name, date = datetime.date.today().strftime('%Y-%m-%d')):
        
        absolute_path = os.path.dirname(__file__)

        ap_split = absolute_path.split('\\')
        i = ap_split.index('capstone')
        del ap_split[i + 1:]
        
        self.cwd = '\\'.join(ap_split)
        
        self.date = date
        
        self.valid_strategies = {'Baseline': self.baseline, 
                                 'Aggressive': self.aggressive, 
                                 'Moderate': self.moderate, 
                                 'Conservative': self.conservative}
        
        if self.is_valid_strategy(strategy):
            self.strategy = strategy
        else:
            raise ValueError()
            
        self.wager = wager
        self.model_name = model_name
        self.feature_set_name = feature_set_name
        self.model = Model(model_name, feature_set_name, date = date)
        
        
    def is_valid_strategy(self, strategy):
        return strategy in self.valid_strategies
   
    
    def optimize(self):
        if self.alreadyOptimized():
            print("\tOptimized distributions previously made.")
            return
        
        self.model.predict()
        
        conn = sqlite3.connect(self.cwd + '\\Database\\'+ 'v1_database.db')
        
        with open(self.cwd + '\\Database\\SQL_Queries\\' + 'query_odd_w_prediction.sql', 'r') as file:
            query = file.read()
        
        df = pd.read_sql(query, conn, params = [self.model_name, self.feature_set_name, self.date, self.date])
        conn.close()
        
        if (len(df) == 0):
            return 
        
        df = df.dropna() #possible unneeded play-off games are here...
        df = df.reset_index(drop=True)
        
        #results needs to be a dataframe
        self.valid_strategies[self.strategy](df, self.wager)
        
        df = df.set_index('game_id')
        df = df.drop(columns = ['prediction_away', 'prediction_home', 'probability_away_win', 'probability_home_win', 'agree?', 'uncertainty', 'profit'])
        
        #format
        #game_id, strategy, total_wager, ind_wager, comment
        df['strategy'] = df.apply(lambda x: self.strategy, axis = 1)
        df['total_wager'] = df.apply(lambda x: self.wager, axis = 1)
        df = df[['date', 'home_team_ml', 'away_team_ml', 'away_team_implied_pct', 'home_team_implied_pct', 'model', 'feature_set', 'decision', 'strategy', 'total_wager', 'ind_wager', 'payout']]
        
        # Write wager distributions to database
        conn = sqlite3.connect(self.cwd + '\\Database\\'+ 'v1_database.db')
        df.to_sql('optimizer_distributions', con = conn, if_exists='append')
        conn.close()

    def alreadyOptimized(self):
        query = """SELECT * 
                    FROM optimizer_distributions
                    WHERE date = ? and strategy = ? and model = ? and feature_set = ? and total_wager = ?"""
        
        with sqlite3.connect(self.cwd + '\\Database\\'+ 'v1_database.db') as conn:
            cur = conn.cursor()
            results = cur.execute(query, [self.date, self.strategy, self.model_name, self.feature_set_name, self.wager]).fetchall()
            
        if len(results) != 0:
            return True
        else:
            return False
            

    def baseline(self, df, wager):
        df['away_team_ml'] = df['away_team_ml'].astype(int)
        df['home_team_ml'] = df['home_team_ml'].astype(int)
        
        df['away_team_implied_pct'] = df['away_team_ml'].apply(calculate_implied_pct)
        df['home_team_implied_pct'] = df['home_team_ml'].apply(calculate_implied_pct)
        df['agree?'] = df.apply(lambda x : add_agree(x.home_team_implied_pct, x.probability_home_win), axis = 1)
        df['uncertainty'] = df['probability_home_win'] - df['home_team_implied_pct'] #vectorizied operation
        df['decision'] = np.where((df['prediction_home'] == 1), df['home_team_ml'], df['away_team_ml'])
        df['ind_wager'] = df.apply(lambda x: add_split_wager(df, x, wager), axis = 1)
        df['profit'] = df.apply(lambda x: add_profit(x), axis = 1)
        df['payout'] = df['ind_wager'] + df['profit']

    
    def aggressive(self, df, wager):
        df['away_team_ml'] = df['away_team_ml'].astype(int)
        df['home_team_ml'] = df['home_team_ml'].astype(int)
        
        df['away_team_implied_pct'] = df['away_team_ml'].apply(calculate_implied_pct)
        df['home_team_implied_pct'] = df['home_team_ml'].apply(calculate_implied_pct)
        df['agree?'] = df.apply(lambda x : add_agree(x.home_team_implied_pct, x.probability_home_win), axis = 1)
        df['bets_of_interest'] = np.where((df['home_team_ml'] >= -200) & (df['home_team_ml'] <= 300), 1, 0)
        df['decision'] = np.where((df['prediction_home'] == 1), df['home_team_ml'], df['away_team_ml'])
        df['uncertainty'] = abs(df['home_team_implied_pct'] - df['probability_home_win'])

        # calculate aggressive wagers
        add_aggressive_wager(df, wager)

        if 'ind_wager' in df:
            df['profit'] = np.where((df['decision'] < 0), 
                                    (100 / abs(df['decision'])) * df['ind_wager'], 
                                    (df['decision'] / 100) * df['ind_wager'])
            df['payout'] = df['ind_wager'] + df['profit']
            df['risk'] = df['uncertainty'] * df['ind_wager']
    

    def conservative(self, df, wager):
        df['away_team_ml'] = df['away_team_ml'].astype(int)
        df['home_team_ml'] = df['home_team_ml'].astype(int)
        
        df['away_team_implied_pct'] = df['away_team_ml'].apply(calculate_implied_pct)
        df['home_team_implied_pct'] = df['home_team_ml'].apply(calculate_implied_pct)
        df['agree?'] = df.apply(lambda x : add_agree(x.home_team_implied_pct, x.probability_home_win), axis = 1)
        df['bets_of_interest'] = np.where((df['home_team_ml'] < -125), 1, 0)
        df['decision'] = np.where((df['prediction_home'] == 1), df['home_team_ml'], df['away_team_ml'])
        df['uncertainty'] = abs(df['home_team_implied_pct'] - df['probability_home_win'])

        # calculate conservative wagers
        add_conservative_wager(df, wager)

        if 'ind_wager' in df:
            df['profit'] = np.where((df['decision'] < 0), 
                                    (100 / abs(df['decision'])) * df['ind_wager'], 
                                    (df['decision'] / 100) * df['ind_wager'])
            df['payout'] = df['ind_wager'] + df['profit']
            df['risk'] = df['uncertainty'] * df['ind_wager']
    
    
    def moderate(self, df, wager):
        df['away_team_ml'] = df['away_team_ml'].astype(int)
        df['home_team_ml'] = df['home_team_ml'].astype(int)
        
        df['away_team_implied_pct'] = df['away_team_ml'].apply(calculate_implied_pct)
        df['home_team_implied_pct'] = df['home_team_ml'].apply(calculate_implied_pct)
        df['agree?'] = df.apply(lambda x : add_agree(x.home_team_implied_pct, x.probability_home_win), axis = 1)
        df['uncertainty'] = abs(df['home_team_implied_pct'] - df['probability_home_win'])
        
        df['decision'] = df.apply(add_alt_decision, axis = 1)
        
        try:
            add_weighted_wager(df, wager)
        except:
            df['ind_wager'] = df.apply(lambda x: add_split_wager(df, x, wager), axis = 1)
        
        df['profit'] = df.apply(lambda x: add_profit(x), axis = 1)
        df['payout'] = df['ind_wager'] + df['profit']
      
# Helper Functions -------------------------------------------------------------
def calculate_implied_pct(num):
    num = int(num)
    
    if num > 0:
        return 100 / (num + 100)
    else:
        return (-1 * num) / ((abs(num)) + 100)
   
    
def add_agree(implied, prob):
    if (implied > 0.5 and prob > 0.5) ^ (implied < 0.5 and prob < 0.5):
        return 'Yes'
    else:
        return 'No'


def add_decision(df):
    agree = df['agree?']
    home_odds = df['home_team_implied_pct']
    home_prediction = df['home_prediction']
    
    if agree == 'Yes' and home_odds > 0.5:
        return df['home_team_ml']
    elif agree == 'Yes' and home_odds < 0.5:
        return df['away_team_ml']
    elif agree == 'No' and home_prediction == 1:
        return df['home_team_ml']
    elif agree == 'No' and home_prediction == 0:
        return df['away_team_ml']
    else:
        return "Undetermined"
    
def add_alt_decision(df):
    agree = df['agree?']
    home_odds = df['home_team_implied_pct']
    home_prediction = df['prediction_home']
    
    if agree == 'Yes' and home_odds > 0.5:
        return df['home_team_ml']
    elif agree == 'Yes' and home_odds < 0.5:
        return df['away_team_ml']
    elif agree == 'No' and home_prediction == 1:
        return df['home_team_ml']
    elif agree == 'No' and home_prediction == 0:
        return df['away_team_ml']
    else:
        return "Undetermined"

def add_split_wager(df, row, wager):
    bet = wager / len(df)

    if not bet.is_integer(): # if not evenly divided
        bet = math.floor(bet)
        
        remainder = wager - bet * len(df)

        lowest_uncertainty = abs(df['uncertainty'])
        lowest_uncertainty = lowest_uncertainty.sort_values(ascending = True).head(remainder)
           
        if abs(row['uncertainty']) in lowest_uncertainty.values:
            return bet + 1
        else:
            return bet
    
    else:
        return bet
    
def get_bets_distribution(n, amt):
    """ Triangle area to determine distribution of bets"""
    if n == 0:
        return []
    
    if amt == 0:
        return n * [0]
    
    height = 2 * amt / n
    m = height / n
    heights = []

    for i in range(1, n + 1):
        heights.append (i * m)
    
    cumulative = []
    for i in range(len(heights)):
        cumulative.append(0.5 * (i + 1) * heights[i])

    bets = []
    for i in range(len(cumulative)):
        if i == 0:
            bets.append(round(cumulative[i], 0))
        else:
            bet = cumulative[i] - cumulative [i - 1]
            bets.append(round(bet, 0))

    if sum(bets) != amt:
        remainder = sum(bets) - amt
        bets[-1] = bets[-1] - remainder

    return bets

def add_aggressive_wager(df, wager):
    # Sum of safer bets must be 30% of inputed wager
    # 70% of wager should go to bets with a better possible payout (between -200 and 300)
    bets_of_interest = df['bets_of_interest']
    bets_of_1 = [i for i in range(len(bets_of_interest)) if bets_of_interest[i] == 1]
    bets_of_0 = [i for i in range(len(bets_of_interest)) if bets_of_interest[i] == 0]
    # distributed wagers from contraints
    better_payout_wagers = get_bets_distribution(len(bets_of_1), math.ceil(wager * 0.7))
    safer_wagers = get_bets_distribution(len(bets_of_0), math.floor(wager * 0.3))
    # sort uncertainty values in ascending order
    uncertainty = df['uncertainty']
    uncertainty_dict = {}
    for i in range(len(uncertainty)):
        uncertainty_dict[i] = uncertainty[i]
    sorted_uncertainty_keys = sorted(uncertainty_dict, key=uncertainty_dict.get)
    # assign wagers based on bets of interest and uncertainty
    wagers = [0.0] * len(df)
    bp_num = 0
    s_num = 0
    for i in range(len(sorted_uncertainty_keys)):
        key = sorted_uncertainty_keys[i]
        if key in bets_of_1:
            wagers[key] = better_payout_wagers[bp_num]
            bp_num += 1
        else:
            wagers[key] = safer_wagers[s_num]
            s_num += 1

    df['ind_wager'] = wagers

def add_conservative_wager(df, wager):
    # Sum of safer bets must be 70% of inputed wager
    # 30% of wager should go to riskier bets (less than -125)
    bets_of_interest = df['bets_of_interest']
    bets_of_1 = [i for i in range(len(bets_of_interest)) if bets_of_interest[i] == 1]
    bets_of_0 = [i for i in range(len(bets_of_interest)) if bets_of_interest[i] == 0]
    # distributed wagers from contraints
    safer_wagers = get_bets_distribution(len(bets_of_1), math.ceil(wager * 0.7))
    riskier_wagers = get_bets_distribution(len(bets_of_0), math.floor(wager * 0.3))
    # sort uncertainty values in descending order
    uncertainty = df['uncertainty']
    uncertainty_dict = {}
    for i in range(len(uncertainty)):
        uncertainty_dict[i] = uncertainty[i]
    sorted_uncertainty_keys = sorted(uncertainty_dict, key=uncertainty_dict.get)
    sorted_uncertainty_keys.reverse()
    # assign wagers based on bets of interest and uncertainty
    wagers = [0.0] * len(df)
    r_num = 0
    s_num = 0
    for i in range(len(sorted_uncertainty_keys)):
        key = sorted_uncertainty_keys[i]
        if key in bets_of_1:
            wagers[key] = safer_wagers[s_num]
            s_num += 1
        else:
            wagers[key] = riskier_wagers[r_num]
            r_num += 1

    df['ind_wager'] = wagers

def add_weighted_wager(df, input_wager):

    # Constants needed ---------------------------------------------------------
    agree = list(df['agree?'])
    decision = list(df['decision'])
    certainty = list(df['uncertainty'])

    # Define the type of solver to use -----------------------------------------
    solver = pywraplp.Solver('Moderate Strategy', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

    # Define variables ---------------------------------------------------------
    infinity = solver.infinity()
    wagers = {}
    max_wager = solver.NumVar(0, infinity, 'max_wager')
    min_wager = solver.NumVar(0, infinity, 'min_wager')

    for i in range(len(df)):
        wagers[i] = solver.IntVar(1, infinity, 'wager[%i]' % i)

    #print('Number of variables =', solver.NumVariables())

    # Define constraints -------------------------------------------------------
    # Sum of all wagers must equal user inputed wager
    solver.Add(
        sum([wagers[i] for i in range(len(wagers))]) == input_wager
        )
    
    max_difference = 4 # <- This number is what controls how spread out the wagers will be
    # Make it so the wagers are more evenly spread out
    for i in range(len(df)):
        solver.Add(max_wager >= wagers[i])
        solver.Add(min_wager <= wagers[i])
    solver.Add(max_wager - min_wager <= max_difference) 

    # Sum of profits from safe bets equals 1.2 times cost of risky bets
    cost = [wagers[i] for x in agree if x == 'No']

    def calc_payout(wager, moneyline):
        if moneyline >= 0:
            return ((moneyline / 100) * wager) + wager
        else:
            return ((100 / abs(moneyline)) * wager) + wager

    cover_cost = [calc_payout(wagers[i], int(decision[i]))  for i, x in enumerate(agree) if x == 'Yes']

    margin = 1.2

    solver.Add(sum(cover_cost) >= margin * sum(cost))

    #print('Number of constraints =', solver.NumConstraints())

    # Define Objective Function ------------------------------------------------
    objective_terms = [wagers[i] * abs(certainty[i]) for i in range(len(df))]

    solver.Minimize(sum(objective_terms))

    # Solve
    status = solver.Solve()

    if status == pywraplp.Solver.OPTIMAL:
        # print('Objective value =', solver.Objective().Value())
        # for j in range(len(df)):
        #     print(wagers[j].name(), ' = ', wagers[j].solution_value())
        # print()

        # print('Problem solved in %f milliseconds' % solver.wall_time())
        # print('Problem solved in %d iterations' % solver.iterations())
        # print('Problem solved in %d branch-and-bound nodes' % solver.nodes())

        df['ind_wager'] = [wagers[i].solution_value() for i in range(len(df))]

    else:
        #print('The problem does not have an optimal solution.')
        raise Exception()

def add_profit(df):
    decision = int(df['decision'])
    wager = df['ind_wager']

    if decision < 0:
        return (100 / abs(decision)) * wager
    else:
        return (decision / 100) * wager


if __name__ == "__main__":
    
    optimizer = Optimizer(25, "Baseline", "knn", "fs1")
    
    optimizer.optimize()
    
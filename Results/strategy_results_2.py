# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 08:50:57 2023

@author: kents
"""

import sys
import pandas as pd
import sqlite3
import math


cwd = r'C:\Users\kents\OneDrive\Desktop\Capstone Local\capstone'

sys.path.insert(0, cwd)
from Optimizer import Optimizer

sys.path.insert(1, cwd + '\\Saved_Models\\TabPFN')
from TabPFN import TabPFNClassifierWrapper


def main():
    
    models = ['TabPFN']
            #['knn', 'logr', 'mlpclassifier', 'lightgbm', 'xgboost', 'adaboost', 'gbt', 'lda', 'randomforest', 'svm', 'naivebayes', 'kmeans, 'TabPFN']
    
    
    feature_sets = ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']
    strategies = ['Baseline', 'Moderate', 'Aggressive', 'Conservative']

    balance_start = {}
    wagers = {}
    balance_end = {}

    for model in models:
        for feature_set in feature_sets:
            
            # Skip svm-fs5 (DNE)
            if model == 'svm' and feature_set == 'fs5':
                continue
            
            for strategy in strategies:
                key = f"{model}-{feature_set}-{strategy}"
                
                balance_start[key], wagers[key], balance_end[key] = results(model, feature_set, strategy)
                print(key)

    return balance_start, wagers, balance_end

def results(model, feature_set, strategy):

    date_range = list(pd.date_range('2023-03-16', '2023-04-19')) #end date cannot be today
        
    starting_balances = []
    wagers = []
    ending_balances = []
    
    with sqlite3.connect(r"C:\Users\kents\OneDrive\Desktop\Capstone Local\capstone\Database\v1_database.db") as conn:
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()
        valid_dates = set(cur.execute("SELECT date from games").fetchall())
        
        for date in date_range:
            date_str = date.strftime('%Y-%m-%d')
            if date_str not in valid_dates:
                date_range.remove(date)
    
    
        for i, date in enumerate(date_range):
            
            date_str = date.strftime('%Y-%m-%d')
                        
            # Starting Wager
            if i == 0:
                starting_balance = 25.0
            else:
                starting_balance = ending_balances[-1]
            
            starting_balances.append(starting_balance)
            
            # Adjust starting wager
            balance = starting_balances[i]
            
            # If profitable set wager to 25
            profit = 0
            savings = 0
            
            if balance >= 25:
                profit = balance - 25
                wager = 25
            
            # If lossing money set wager to half of balance
            elif balance >= 2 and balance < 15:
                new_wager = math.floor(balance / 2)
                savings = balance - new_wager
                
                wager = new_wager
            
            elif balance >= 1 and balance < 2:
                savings = balance - 1.0
                wager = 1.0
            
            elif balance < 1:
                savings = balance
                wagers.append(0)
                ending_balances.append(round(savings, 2))
                continue
            else:
                wager = int(balance)
                savings = balance - int(balance)

        
            # Run model and optimizer
            wagers.append(int(wager))
            optimizer = Optimizer(wagers[i], strategy, model, feature_set, date = date_str)
            optimizer.optimize()
            
            # Get ending balance
            query = """SELECT * FROM results_agg"""
            
            conn.row_factory = None
            df_results = pd.read_sql(query, conn)
            
            df = df_results.query(f"date == '{date_str}' and model == '{model}' and feature_set == '{feature_set}' and strategy == '{strategy}' and total_wager == {wagers[i]}" )
            
            ending_balances.append(round(df['balance'].values[0] + profit + savings, 2))
            
        return starting_balances, wagers, ending_balances
    
if __name__ == "__main__":
    start, wagers, end = main()
    
    start = pd.DataFrame(start).transpose()
    wagers = pd.DataFrame(wagers).transpose()
    end = pd.DataFrame(end).transpose()
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 12:22:37 2023

@author: kents
"""
import sqlite3
import pandas as pd
import matplotlib.pyplot as plt

sims = {
        'sim1': {
            'plot1': ['gbt-fs4-Aggressive', 'mlpclassifier-fs3-Moderate'],
            },
        
        'sim2': {
            'plot1': ['gbt-fs4-Aggressive', 'knn-fs4-Baseline'],
            },
        
        'sim3': {
            'plot1': ['gbt-fs4-Aggressive', 'lightgbm-fs4-Baseline', 'xgboost-fs2-Moderate']
            }
        
        }

query = """SELECT 
        	games.game_id,
        	games.date,
        	t1.name,
        	t2.name,
        	CAST(odds.away_team_ml as INT) as 'away_team_ml',
        	CAST(odds.home_team_ml as INT) as 'home_team_ml',
        	games.outcome,
        	CASE
        		WHEN CAST(odds.home_team_ml as INT) < 0 and games.outcome = 'win'
        			THEN 0
        		WHEN CAST(odds.home_team_ml as INT) < 0 and games.outcome = 'loss'
        			THEN 1
        		WHEN CAST(odds.home_team_ml as INT) > 0 and games.outcome = 'win'
        			THEN 1	
        		WHEN CAST(odds.home_team_ml as INT) > 0 and games.outcome = 'loss'
        			THEN 0
        	END as 'upset'
        
        FROM games
        
        LEFT JOIN teams as t1 on (t1.team_id = games.away_team_id)
        LEFT JOIN teams as t2 on (t2.team_id = games.home_team_id)
        
        INNER JOIN odds on (odds.date = games.date and odds.away_team = t1.name and odds.home_team = t2.name)
        
        WHERE games.date >= '2023-03-16' and games.date <= '2023-04-19'
        
        ORDER BY games.date"""

with sqlite3.connect(r"C:\Users\kents\OneDrive\Desktop\Capstone Local\capstone\Database\v1_database.db") as conn:
    df_upsets = pd.read_sql(query, conn)

df_upsets_agg = df_upsets.groupby('date').agg({'upset': 'sum'})
df_upsets_agg = df_upsets_agg.reset_index(drop=True)

# Plot 1 ----------------------------------------------------------------------


for key, value in sims.items():
    
    df = pd.read_excel(r"C:\Users\kents\OneDrive\Desktop\Capstone Local\capstone\Results\results.xlsx", sheet_name = f'{key}_end', index_col = 0)
    dfm = df.melt(ignore_index=False).reset_index()
    
    fig, ax = plt.subplots()
    
    ax2 = ax.twinx()
    
    ax2.bar(df_upsets_agg.index, df_upsets_agg['upset'], alpha = 0.5, align = 'center', color = 'gold')
    ax2.set_ylim(bottom = 0, top = 15)
    ax2.set_ylabel('Upsets (Count)')
   
    for plot in value['plot1']:
        df_query = dfm.query(f"index == '{plot}'")
        ax.plot(df_query['variable'], df_query['value'], label = f"{plot}")
    
    ax.plot([0, max(df_query['variable'])], [25, 25], color = 'grey', linestyle = '--', alpha = 0.75, label = 'Starting Balance')
    
    

    
    ax.set_xlim(left = 0, right = max(df_query['variable']))
    ax.set_ylim(bottom = 0)
        
    ax.set_title(f'{key}')
    ax.set_xlabel('Day')
    ax.set_ylabel('Ending Balance (USD)')
    ax.legend(loc='center left', bbox_to_anchor=(1.1, 0.5), frameon = False)
    
    
    fig.set_size_inches(7.5, 5)
    plt.savefig(f'C:\\Users\\kents\\OneDrive\\Desktop\\{key}.png', bbox_inches='tight', dpi=300)
    

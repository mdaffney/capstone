# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 08:28:34 2023

@author: kents
"""

import os
import sqlite3
import pandas as pd
import numpy as np
from alive_progress import alive_bar

from datetime import datetime, timedelta, date


absolute_path = os.path.dirname(__file__)

# Determine relative starting path
ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)
database = 'v1_database.db'

del ap_split, i

#%% Training Set

query1 = """
            SELECT
                game_id
            FROM games
            WHERE date <= '2023-3-15' and status == 'Finished'
        """

query2 = open(cwd + '\\Saved_Feature_Sets\\FS6\\query_fs6_2.sql', 'r').read()

with sqlite3.connect(cwd + '\\Database\\' + database) as conn:

    conn.row_factory = lambda cursor, row: row[0]
    cur = conn.cursor()
    games = cur.execute(query1).fetchall()
    
    conn.row_factory = None
    df_boxscores = pd.read_sql(query2, con = conn)

    df_boxscores['outcome'] = df_boxscores['outcome'].replace({'win': 1, 'loss': 0})

    # Get 10 players from each team with the most mins (sorted in query2)
    df_boxscores = df_boxscores.groupby(['game_id', 'team_id']).head(8)

col_names = ['game_id', 'away_team_id', 'home_team_id', 'outcome']

for status in ['away', 'home']:
    for i in range(1, 9):
        for stat in ['fga', 'fgm', '3fga', '3fgm', 'ftm', 'defReb', 'totReb', 'turnovers', 'pFouls']:
            col_names.append(f'{status}_{stat}_{i}')


df_final = pd.DataFrame(columns = col_names)

with alive_bar(len(games)) as bar:
    for game in games:
        
        df_boxscore = df_boxscores.query(f"game_id == {game}")
        
        #ignore games where less than 8 players play per team
        if len(df_boxscore) != 16:
            bar()
            continue
        
        game_id = list(df_boxscore.game_id.unique())
        team_ids = list(df_boxscore.team_id.unique())
        outcome = list(df_boxscore.outcome.unique())
        
        index = game_id + team_ids + outcome
        
        x = df_boxscore.drop(columns = ['game_id', 'team_id', 'player_id', 'home_or_away', 'outcome'])
    
        x = x.to_numpy().flatten()
        
        final = np.insert(x, 0, values = index)
    
        df_final.loc[len(df_final)] = final
        
        bar()

if os.path.isfile(absolute_path + '\\fs6.pickle'):
    os.remove(absolute_path + '\\fs6.pickle')

df_final.to_pickle(absolute_path + '\\fs6.pickle')


#%% Prediction Set

query1 = """
            SELECT
                *
            FROM games
            WHERE date = ? and status != 'Ignore'
        """

query2 = """
            SELECT 
                player_id
    
            FROM boxscores 
    
            LEFT JOIN games on (games.game_id = boxscores.game_id)
    
            WHERE status = 'Finished' and team_id = ? and date >= ? and date < ?
    
            GROUP BY player_id
    
            ORDER BY min DESC LIMIT 8
        """


query3 = """
            SELECT * FROM player_averages WHERE season = 2022 and player_id = ?
        """

end_date = datetime.today()
start_date = end_date - timedelta(days = 10)

with sqlite3.connect(cwd + '\\Database\\' + database) as conn:

    cur = conn.cursor()
    games = cur.execute(query1, [end_date.strftime('%Y-%m-%d')]).fetchall()
    
    conn.row_factory = lambda cursor, row: row[0]
    cur = conn.cursor()
    
    df_pred = pd.DataFrame(columns = col_names)
    
    for game_id, season, date, home_team_id, away_team_id, home_team_pts, away_team_pts, status, outcome in games:
        
        away_players = cur.execute(query2, [away_team_id, start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')]).fetchall()
        home_players = cur.execute(query2, [home_team_id, start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')]).fetchall()
        
        players = away_players + home_players
        
        conn.row_factory = None
        x = [game_id, away_team_id, home_team_id, outcome]
        for player in players:
            stats = pd.read_sql(query3, conn, params = [player])
            
            stats = stats.drop(columns = ['player_id', 'season', 'AVG(min)'])
            
            x = x + [x.values[0] for i, x in stats.iteritems()]
            
        df_pred.loc[len(df_pred)] = x
            

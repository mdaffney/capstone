--FGA, FGP, TPP, FT, DRB, TRB, TOV, PF

SELECT
	boxscores.game_id,
	boxscores.team_id,
	CASE
		WHEN team_id = home_team_id THEN 'home' ELSE 'away'
	END as 'home_or_away',
	boxscores.player_id,
	--min,
	fga,
	fgm,
	boxscores.'3fga',
	boxscores.'3fgm',
	ftm,
	defReb,
	totReb,
	turnovers,
	pFouls,
	games.outcome

FROM 
boxscores
LEFT JOIN games on (games.game_id = boxscores.game_id)

ORDER BY boxscores.game_id ASC, home_or_away ASC, min DESC
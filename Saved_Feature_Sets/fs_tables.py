# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 09:03:25 2023

@author: kents
"""

# Packages
import sys
import os
import pandas as pd
import dataframe_image as dfi

import warnings
warnings.filterwarnings("ignore")

# File location information

absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

database = 'v1_database.db'

# Our project classes
sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

fsb = FeatureSetBuilder()



# Feature Sets -------------------------------------------------------------
features_list, x, y = fsb.get('fs1')

index, x_pred = fsb.get_prediction_set('fs1')

features = pd.DataFrame(x, columns = features_list).head(5)
features['...'] = features.apply(lambda x: "...", axis = 1)

features = features[['away_pts', 'away_fgm', 'away_fgp', '...', 'home_steals', 'home_turnovers', 'home_blocks']]
label = pd.DataFrame(y, columns = ['outcome']).head(5)


prediction = pd.DataFrame(x_pred, columns = features_list)
prediction['...'] = prediction.apply(lambda x: "...", axis = 1)
prediction = prediction[['away_pts', 'away_fgm', 'away_fgp', '...', 'home_steals', 'home_turnovers', 'home_blocks']] 

dfi.export(features,"fs1_features.png")
dfi.export(label,"fs1_label.png")

dfi.export(index, 'fs1_prediction_index.png')
dfi.export(prediction,"fs1_prediction.png")

# Prediction Sets ----------------------------------------------------------
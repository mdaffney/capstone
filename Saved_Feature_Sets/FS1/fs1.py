# -*- coding: utf-8 -*-
"""

Generates a pickle file to be read in by FeatureSetBuilder for Feature Set 1.
Feature Set 1 consists records representing Away vs. Home Statistics and the 
outcome of those games.


Created on Thu Mar 23 08:23:39 2023

@author: kents
"""

import os
import sqlite3
import pandas as pd

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)
database = 'v1_database.db'

del ap_split, i

# Query database
query = open(absolute_path + '\\query_fs1.sql', 'r')

conn = sqlite3.connect(cwd + '\\Database\\' + database)
df = pd.read_sql(query.read(), conn)
conn.close()

del query

# Save data to pickle
if os.path.isfile(absolute_path + '\\fs1.pickle'):
    os.remove(absolute_path + '\\fs1.pickle')

df.to_pickle(absolute_path + '\\fs1.pickle')


SELECT DISTINCT
	games.game_id,
	season,
	date,
	away_team_id,
	home_team_id,
	outcome
FROM games 

INNER JOIN boxscores on boxscores.game_id = games.game_id
       
WHERE season >= 2020 and status = 'Finished' and date <= '2023-03-15'
            
ORDER BY date, games.game_id
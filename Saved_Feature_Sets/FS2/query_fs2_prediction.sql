SELECT
	game_id,
    season,
    date,
	away_team_id,
	home_team_id,
	outcome
FROM games 
            
WHERE status != 'Ignore' and date >= ? and date <= ?
            
ORDER BY date, game_id
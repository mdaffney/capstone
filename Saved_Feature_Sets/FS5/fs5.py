# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 11:35:18 2023

@author: kents
"""

import sys
import os
import pandas as pd
import sqlite3
from alive_progress import alive_bar

database = 'v1_database.db'

absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

sys.path.insert(0, cwd + '\\Database')
import MiscFeatures

# %% Data

query1 = open(cwd + '\\Database\\temp\\fs1_games_team.sql', 'r').read()
query2 = """SELECT * FROM stadiums"""

with sqlite3.connect(cwd + '\\Database\\' + database) as conn:
    games_by_team = pd.read_sql(query1, conn).sort_values(['date', 'game_id'])
    games_by_team = games_by_team.reset_index()
    games_by_team['date'] = pd.to_datetime(games_by_team['date'], format = '%Y-%m-%d')
    games_by_team = games_by_team.dropna()

    stadiums = pd.read_sql(query2, conn)


games_by_team['days_since_last_game'] = games_by_team.apply(lambda x: MiscFeatures.days_since_last_game(games_by_team, x.team_id, x.date, x.season), axis = 1)
   
games_by_team['lat_long'] = games_by_team.apply(lambda x: MiscFeatures.location_played(stadiums, x.home_away, x.team_id, x.opponent_id), axis = 1)
games_by_team[['lat', 'long']] = pd.DataFrame(games_by_team['lat_long'].tolist(), index = games_by_team.index)
games_by_team = games_by_team.drop(columns = ['lat_long'])

games_by_team['dist_traveled'] = games_by_team.apply(lambda x: MiscFeatures.distance_from_last_game(games_by_team, x.team_id, x.date, x.season, x.lat, x.long), axis = 1)

games_by_team['ELO_before'] = games_by_team.apply(lambda x: 1500, axis = 1)
games_by_team['ELO_after'] = games_by_team.apply(lambda x: 1500, axis = 1)

with alive_bar(len(games_by_team)) as bar:
    for i, row in games_by_team.iterrows():
        elo_b, elo_a = MiscFeatures.elo_calculation(games_by_team, row)
        games_by_team.loc[i, 'ELO_before'] = elo_b
        games_by_team.loc[i, 'ELO_after'] = elo_a
        bar()
    
    
# %% Add to database
# with sqlite3.connect(cwd + '\\Database\\' + database) as conn:
#     games_by_team = games_by_team.drop(columns = ['index'])
#     games_by_team = games_by_team.set_index(['game_id', 'team_id'])
#     games_by_team.to_sql('games_by_team', con = conn, if_exists='replace')
    

# %% Feature Set 5

games_by_team = games_by_team.query("date <= '2023-03-15'")

games_by_away_team = games_by_team.query("home_away == 'away' and season >= 2020")[['game_id', 'days_since_last_game', 'dist_traveled', 'ELO_before']]
games_by_home_team = games_by_team.query("home_away == 'home'and season >= 2020")[['game_id', 'days_since_last_game', 'dist_traveled', 'ELO_before']]

if os.path.isfile(cwd + '\\Saved_Feature_Sets\\FS2\\' + 'fs2.pickle'):
    fs2 = pd.read_pickle(cwd +'\\Saved_Feature_Sets\\FS2\\' + 'fs2.pickle')
    fs2 = fs2.rename(columns = {0: 'game_id'})
    
    fs2 = fs2.drop(fs2.columns[[6, 7, 8, 9, 27, 28, 29, 30]], axis = 1)
    
    fs2.columns =  ['game_id', 'season', 'date', 'away_team_id', 'home_team_id', 'outcome', 
                    'away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                    'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                    'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                    'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks']
    
    
    merged = pd.merge(fs2, games_by_away_team, on = 'game_id')
    merged = pd.merge(merged, games_by_home_team, on = 'game_id', suffixes = ("_away","_home"))

        
if os.path.isfile(absolute_path + '\\fs5.pickle'):
    os.remove(absolute_path + '\\fs5.pickle')

merged.to_pickle(absolute_path + '\\fs5.pickle')

# -*- coding: utf-8 -*-
"""

Generates a pickle file to be read in by FeatureSetBuilder for Feature Set 4.
Feature Set 4 consists of a combination of feature sets 2 and 3.

Created on Thu Mar 23 09:29:07 2023

@author: kents
"""

import os
import pandas as pd

absolute_path = os.path.dirname(__file__)

# Determine relative starting path
ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)
database = 'v1_database.db'

del ap_split, i


# Check if fs2 and fs3 files exist
if os.path.isfile(cwd + '\\Saved_Feature_Sets\\FS2\\' + 'fs2.pickle') and os.path.isfile(cwd + '\\Saved_Feature_Sets\\FS3\\' + 'fs3_10_10.pickle'):
    
    fs2 = pd.read_pickle(cwd +'\\Saved_Feature_Sets\\FS2\\' + 'fs2.pickle')
    fs3 = pd.read_pickle(cwd +'\\Saved_Feature_Sets\\FS3\\' + 'fs3_10_10.pickle')
    
    df = pd.concat([fs2, fs3], axis = 1)
    
    df.columns = range(df.columns.size)
    df = df.drop(df.columns[[6, 7, 8, 9, 
                             27, 28, 29, 30, 
                             48, 49, 50, 51, 52, 53,
                             54, 55, 56, 57,
                             75, 76, 77, 78]], axis = 1)
    
    df.columns =  ['game_id', 'season', 'date', 'away_team_id', 'home_team_id', 'outcome',
                         
                   'away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                   'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                         
                   'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                   'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks',
                   
                   'away_pts_10', 'away_fgm_10', 'away_fga_10', 'away_fgp_10', 'away_ftm_10', 'away_fta_10', 'away_ftp_10', 'away_3fgm_10', 'away_3fga_10', 'away_fg3p_10', 'away_offReb_10', 
                   'away_defReb_10', 'away_assists_10', 'away_pFouls_10', 'away_steals_10', 'away_turnovers_10', 'away_blocks_10', 
                         
                   'home_pts_10', 'home_fgm_10', 'home_fga_10', 'home_fgp_10', 'home_ftm_10', 'home_fta_10', 'home_ftp_10', 'home_3fgm_10', 'home_3fga_10', 'home_fg3p_10', 'home_offReb_10', 
                   'home_defReb_10', 'home_assists_10', 'home_pFouls_10', 'home_steals_10', 'home_turnovers_10', 'home_blocks_10'
                   ]
    
    
    
# Save data to pickle
if os.path.isfile(absolute_path + '\\fs4_10_10.pickle'):
    os.remove(absolute_path + '\\fs4_10_10.pickle')

df.to_pickle(absolute_path + '\\fs4_10_10.pickle')
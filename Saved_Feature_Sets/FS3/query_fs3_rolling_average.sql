SELECT 
	test.season,
	test.date,
	team_id,
	home_v_away,
	AVG(pts),
	AVG(fgm),
	AVG(fga),
	SUM(fgm) / SUM (fga) as 'fgp',
	AVG(ftm),
	AVG(fta),
	SUM(ftm) / SUM (fta) as 'ftp',
	AVG(test.'3fgm'),
	AVG(test.'3fga'),
	SUM(test.'3fgm') / SUM (test.'3fga') as 'fg3p',
	AVG(offReb),
	AVG(defReb),
	AVG(assists),
	AVG(pFouls),
	AVG(steals),
	AVG(turnovers),
	AVG(blocks)

FROM 

(SELECT * FROM boxscores_team
LEFT JOIN games on (games.game_id = boxscores_team.game_id)

WHERE team_id = ? and home_v_away = ? and games.season = ? and date < ?

ORDER BY date DESC LIMIT ?) as test

GROUP BY test.season
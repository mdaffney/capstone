# -*- coding: utf-8 -*-
"""

Generates a pickle file to be read in by FeatureSetBuilder for Feature Set 3.
Feature Set 3 consists of records representing the rolling averages for 'i' 
and 'j' number of games for Away vs. Home Statistics and the outcome of those 
games, respectively.

For example a record may compare the rolling average for the home teams last
7 games to the rolling average for the away teams last 5 games.

Created on Thu Mar 23 08:24:05 2023

@author: kents
"""

import os
import sqlite3
import pandas as pd
from alive_progress import alive_bar

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)
database = 'v1_database.db'

del ap_split, i

# Query database & build feature set
query1 = open(absolute_path + '\\query_fs3_games.sql', 'r').read()
query2 = open(absolute_path + '\\query_fs3_rolling_average.sql', 'r').read()
query3 = "SELECT team_id FROM teams"

conn = sqlite3.connect(cwd + '\\Database\\' + database)
cur = conn.cursor()

games = cur.execute(query1).fetchall()

del query1

away_list = []
home_list = []

# Limitied empirical testing showed decent results
limit_away = 5
limit_home = 5

print('Querying database...')
with alive_bar(len(games)) as bar:
    for i, (game_id, season, date, away_team_id, home_team_id, outcome) in enumerate(games):
        
        away = cur.execute(query2, [away_team_id, "away", season, date, limit_away]).fetchone()
        home = cur.execute(query2, [home_team_id, "home", season, date, limit_home]).fetchone()
        
        # Accounts for the beginning of seasons
        if away is None:
            away = (season, date, away_team_id, "away",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            
        if home is None:
            home = (season, date, home_team_id, "home",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            
        away_list.append(away)
        home_list.append(home)
        
        bar()
    
    
del i, game_id, season, date, away_team_id, home_team_id, outcome, away, home, query2

df_games = pd.DataFrame(games)
df_away = pd.DataFrame(away_list)
df_home = pd.DataFrame(home_list)
df_final = pd.concat([df_games, df_away, df_home], axis = 1)
df_final.columns = range(df_final.columns.size)

del games, away_list, home_list, df_games, df_away, df_home

# Drop the first 'n' number of games per season per team... let averages settle
n = 7 

teams = cur.execute(query3).fetchall()
del query3

conn.close()

seasons = df_final[df_final.columns[1]].unique()
teams = [result[0] for result in teams]

print('Dropping first %s home/away games...' % n)

with alive_bar(len(seasons) * len(teams)) as bar:
    for season in seasons:
        for team in teams:

            index_to_drop = df_final.query("(@df_final[1] == %s) and (@df_final[3] == %s)" % (season, team)).head(n).index
            df_final.drop(index_to_drop, inplace = True)
            
            index_to_drop = df_final.query("(@df_final[1] == %s) and (@df_final[4] == %s)" % (season, team)).head(n).index
            df_final.drop(index_to_drop, inplace = True)
            
            bar()

del n, season, seasons, team, teams, index_to_drop

# Save data to pickle
if os.path.isfile(absolute_path + '\\fs3_%s_%s.pickle' % (limit_away, limit_home)):
    os.remove(absolute_path + '\\fs3_%s_%s.pickle' % (limit_away, limit_home))

df_final.to_pickle(absolute_path + '\\fs3_%s_%s.pickle' % (limit_away, limit_home))
SELECT
	game_id,
    season,
    date,
	away_team_id,
	home_team_id,
	outcome
FROM games 
            
WHERE status = 'Finished'
            
ORDER BY date, game_id
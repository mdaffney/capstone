# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 10:27:53 2023

@author: kents
"""
import os
import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


start = datetime.date(2023, 1, 17)
end = datetime.date(2023, 5, 5)
delta = end - start

days = range(0, delta.days)

absolute_path = os.path.dirname(__file__)

excel_file = absolute_path + '\\burndown data.xlsx'

burndown_data = pd.read_excel(excel_file, sheet_name = 'New')

burndown_data['day'] = burndown_data['Date'].apply(lambda x: (x.date() - start).days)

# Remaining Effort
total_effort = sum(burndown_data['Est Time (days)'])

temp = {'day': [], 'effort remaining': []}

for day in days:
    temp['day'].append(day)
    
    temp['effort remaining'].append(
        total_effort - sum(burndown_data.query("day <= %d" % day)['Est Time (days)'])
        )

effort_remaining = pd.DataFrame(temp)

# Tasks Completed
tasks_completed = burndown_data.query("Status == 'Complete'").groupby('day', sort = True).agg({'Task': 'count'})

# Ideal Burndown
x = [0, max(days)]
y = [total_effort, 0]


# # Today 
# x2 = [(datetime.date.today() - start).days, (datetime.date.today() - start).days]
# y2 = [0, 25]

# Specific day
x2 = [(datetime.date(2023, 4, 30) - start).days, (datetime.date(2023, 4, 30) - start).days]
y2 = [0, 25]

# Plot
fig, ax = plt.subplots()



# Tasks Completed Bar Chart
ax2 = ax.twinx()
l3 = ax2.bar(tasks_completed.index, tasks_completed['Task'], color = 'gold', label = 'Tasks Completed')

# Ideal Burndown
l1 = ax.plot(x, y, label = 'Ideal Burndown')

# Actual Burndown
l2 = ax.plot(effort_remaining['day'], effort_remaining['effort remaining'], label = 'Remaining Effort')


# Today
l4 = ax2.plot(x2, y2, '--', label = 'Today', c = 'red', alpha = 0.5)

# Combine Legends
lines, labels = ax.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='center left', bbox_to_anchor=(1.1, 0.5), frameon = False)


# Sprints
ax.axvspan(xmin = 0, xmax = 40, ymin=0, ymax=25, alpha = 0.1, color = 'grey')
ax.axvspan(xmin = 75, xmax = 96, ymin=0, ymax=25, alpha = 0.1, color = 'grey')


# Formatting
#fig.suptitle('Burndown Chart')
ax.set_xlabel('Day')
ax.set_ylabel('Time (days)')
ax2.set_ylabel('Task Count')

ax.set_xlim(0, 107)

ax.set_ylim(bottom = 0)
ax2.set_ylim(0, 25)


plt.savefig('burndown_chart.png', bbox_inches='tight', dpi=600)
plt.show()
# Capstone: Sports Betting Optimization with Machine Learning Algorithms

## Team Members: 
Daffney Myers, Kent Sullivan, Alicia Harman, and Dhiraj Srivastava

## Objective:
The team aims to create an automated framework that retrieves data, builds and updates a database, and compares the predicted outcome of multiple machine learning algorithms against daily betting odds. The initial system will evaluate NBA basketball, but could be expanded to analyze other sports. This project also aims to experiment with different feature sets as well as different types of machine learning algorithms to determine the best predictions.

## Models Used:
- Random Forest 
- Adaptive boosting 
- Gradient boosting
- K-Means
- K-Nearest Neighbor
- Linear Discriminant Analysis
- Light gradient boosting machine
- Logistical Regression
- Multi-layer Perceptron
- Naive Bayes
- Support Vector Machine
- Extreme Gradient Boosting
- Tabular Prior-Data Fitted Network (TabPFN)
- Extremely Boosted Neural Network (XBNet)

## Optimizer Strategies:
- Baseline
- Moderate
- Aggresive
- Conservative

## Information
For more in depth information on the project please visit the project wiki pages at https://code.vt.edu/mdaffney/capstone/-/wikis/home. 

There will be various pages with better explanations, which can be used to change and better the applied methods. Included in User Information is a working demo for the project as it was intended to run and be used.

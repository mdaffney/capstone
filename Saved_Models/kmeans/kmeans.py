import sys
import os
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
import pickle

import warnings
warnings.filterwarnings("ignore")

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder


for feature_set in ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']:
    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
    
    # Define the KMeans model
    kmeans = KMeans(n_clusters = 2)
    kmeans.fit(x_train)
    train_cluster_labels = kmeans.predict(x_train)
    
    # Train an MLPClassifier on the clustered data
    model = MLPClassifier(hidden_layer_sizes=(20, 5), max_iter=1000)
    model.fit(x_train, train_cluster_labels)
    
    # Predict cluster labels for the test set using the KMeans model
    test_cluster_labels = kmeans.predict(x_test)
    
    # Predict labels for the test set using the trained MLPClassifier
    y_pred = model.predict(x_test)
    
    # Calculate the accuracy on the training set
    train_accuracy = accuracy_score(train_cluster_labels, model.predict(x_train))
    print("Training set accuracy: ", train_accuracy)
    
    # Calculate the accuracy on the test set
    test_accuracy = accuracy_score(y_test, y_pred)
    print("Test set accuracy: ", test_accuracy)
    
    # Save the best model
    filename =  f'kmeans_{feature_set}.sav'
    
    with open(filename, 'wb') as file:
        pickle.dump(model, file)


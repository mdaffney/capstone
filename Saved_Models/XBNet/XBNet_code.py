import os
import sys
import torch
import numpy as np
import pickle

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from XBNet.training_utils import training, predict
from XBNet.models import XBNETClassifier
from XBNet.run import run_XBNET
from sklearn.metrics import accuracy_score

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

import warnings
warnings.filterwarnings("ignore")


for feature_set in ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']:

    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    le = LabelEncoder()
    y_data = np.array(le.fit_transform(y))
    
    X_train, X_test, y_train, y_test = train_test_split(x, y_data, test_size = 0.2, random_state = 4) #x.to_numpy()
    model = XBNETClassifier(X_train,y_train,2)
    # Train the model
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
    m, acc, lo, val_ac, val_lo = run_XBNET(X_train, X_test, y_train, y_test, model, criterion, optimizer, 16, 10)
    
    
    # Save the trained model to a .sav file
    filename = f'XBNet_{feature_set}.sav'
    pickle.dump(m, open(filename, 'wb'))
    
    loaded_model = pickle.load(open(filename, 'rb'))
    
    test_accuracy = accuracy_score(y_test, (predict(loaded_model, X_test)))
    
    # Print the test accuracy
    print("Test Accuracy:", test_accuracy)


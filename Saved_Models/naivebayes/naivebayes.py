import sys
import os

from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from scipy.stats import uniform
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import accuracy_score
import pickle

import warnings
warnings.filterwarnings("ignore")

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

for feature_set in ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']:
    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
    
    # Define the Naive Bayes model
    nb = GaussianNB()
    
    # Define the hyperparameter distribution to search over
    param_dist = {'var_smoothing': uniform(1e-9, 1e-7),'priors': [None, [0.2, 0.8], [0.5, 0.5], [0.8, 0.2]],}
    
    # Use RandomizedSearchCV to find the best hyperparameters
    random_search = RandomizedSearchCV(nb, param_distributions=param_dist, n_iter=10, cv=3)
    random_search.fit(x_train, y_train)
    
    # Print the best hyperparameters and accuracy score
    print("Best hyperparameters: ", random_search.best_params_)
    print("Best accuracy score: ", random_search.best_score_)
    
    # Save the best trained model in .sav format
    best_model = random_search.best_estimator_
    #joblib.dump(best_model, 'naivebayes_fs1.sav')
    
    filename = f'naivebayes_{feature_set}.sav'
    pickle.dump(best_model, open(cwd + '\\Saved_Models\\naivebayes\\' + filename, 'wb'))
    
    # Load the saved model
    #loaded_model = joblib.load('naivebayes_fs1.sav')
    loaded_model = pickle.load(open(filename, 'rb'))
    
    # Make predictions on the test set and calculate accuracy
    y_pred = loaded_model.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    print("Test set accuracy: ", accuracy)

import os
import sys
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import uniform
import pickle

import warnings
warnings.filterwarnings('ignore')

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder


for feature_set in ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']:
    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
    
    # Define the SVM model
    svm = SVC(probability=True)
    
    # Define the hyperparameter distribution to search over
    param_dist = {'C': uniform(0.1, 10),'kernel': ['linear', 'rbf'], 'degree': [2, 3],'gamma': ['scale', 'auto'] + list(uniform(0.1, 10).rvs(5)),'coef0': uniform(-1, 1)}
    
    # Use RandomizedSearchCV to find the best hyperparameters
    random_search = RandomizedSearchCV(svm, param_distributions=param_dist, n_iter=10, cv=5, verbose=3)
    random_search.fit(x_train, y_train)
    
    # Print the best hyperparameters and accuracy score
    print("Best hyperparameters: ", random_search.best_params_)
    print("Best accuracy score: ", random_search.best_score_)
    
    # Save the best trained model in .sav format
    best_model = random_search.best_estimator_
    # Save the best model
    
    filename = f'svm_{feature_set}.sav'
    
    with open(filename, 'wb') as file:
        pickle.dump(best_model, file)
    
    # Load the saved model
    with open(filename, 'rb') as file:
        loaded_model = pickle.load(file)
    
    
    # Make predictions on the test set and calculate accuracy
    y_pred = loaded_model.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    print("Test set accuracy: ", accuracy)

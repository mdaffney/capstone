import os
from pathlib import Path
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import sqlite3
from sklearn.impute import SimpleImputer
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

from sklearn.metrics import accuracy_score

import sys


# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler
from scipy.stats import randint, uniform
import joblib
from sklearn.metrics import accuracy_score

import pickle

import warnings
warnings.filterwarnings("ignore")

# Get data
fsb = FeatureSetBuilder()
features, x, y = fsb.get('fs1')

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)

# Define the AdaBoost model
adaboost = AdaBoostClassifier()

# Define the hyperparameter distribution to search over
param_dist = {'n_estimators': randint(50, 500),  'learning_rate': uniform(0.001, 0.1),'algorithm': ['SAMME', 'SAMME.R'], 'random_state': randint(1, 100)}

# Use RandomizedSearchCV to find the best hyperparameters
random_search = RandomizedSearchCV(adaboost, param_distributions=param_dist, n_iter=10, cv=5)
random_search.fit(x_train, y_train)

# Print the best hyperparameters and accuracy score
print("Best hyperparameters: ", random_search.best_params_)
print("Best accuracy score: ", random_search.best_score_)

# Save the best trained model in .sav format
best_model = random_search.best_estimator_
# Save the best model
with open('adaboost_fs1.sav', 'wb') as file:
      pickle.dump(best_model, file)

# Load the saved model
with open('adaboost_fs1.sav', 'rb') as file:
    loaded_model = pickle.load(file)


# Make predictions on the test set and calculate accuracy
y_pred = loaded_model.predict(x_test)
accuracy = accuracy_score(y_test, y_pred)
print("Test set accuracy: ", accuracy)

import sys
import os
import numpy as np

from tabpfn import TabPFNClassifier
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import accuracy_score
import pickle

import warnings
warnings.filterwarnings('ignore')

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder


class TabPFNClassifierWrapper(BaseEstimator, ClassifierMixin):
    def __init__(self, device='cpu', N_ensemble_configurations=1, overwrite_warning=False):
        self.device = device
        self.N_ensemble_configurations = N_ensemble_configurations
        self.overwrite_warning = overwrite_warning
        self.clf = TabPFNClassifier(device=self.device, N_ensemble_configurations=self.N_ensemble_configurations)

    def fit(self, X, y):
        self.clf.fit(X, y, overwrite_warning=self.overwrite_warning)
        return self

    def predict(self, X):
        return self.clf.predict(X)
    
    def predict_proba(self, X):
        return self.clf.predict_proba(X)

    def score(self, X, y):
        y_pred = self.predict(X)
        return accuracy_score(y, y_pred)

if __name__ == '__main__':
    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get('fs3')
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
       
    param_dist = {
        'N_ensemble_configurations': np.arange(1, 32, dtype=int) 
    }
    
    clf = TabPFNClassifierWrapper(overwrite_warning=True)
    
    random_search = RandomizedSearchCV(
        estimator=clf,
        param_distributions=param_dist,
        n_iter=10,  
        cv=3,  
        n_jobs=-1, 
        verbose=1
    )
    
    random_search.fit(x_train, y_train)
    best_model = random_search.best_estimator_
    
    test_score = best_model.score(x_test, y_test)
    print(f"Best model test score: {test_score}")
    
    # Save the best model
    with open('TabPFN_fs3.sav', 'wb') as file:
        pickle.dump(best_model, file)
        
import os
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import sqlite3
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

import sys


# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler
from scipy.stats import randint, uniform
import joblib
from sklearn.metrics import accuracy_score

import pickle

import warnings
warnings.filterwarnings("ignore")

feature_set = 'fs5'

# Get data
fsb = FeatureSetBuilder()
features, x, y = fsb.get(feature_set)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)

# Define the logistic regression model
lr = LogisticRegression()

# Define the hyperparameter distribution to search over
param_dist = {'C': uniform(0.1, 10), 'penalty': ['l1', 'l2'], 'solver': ['liblinear', 'saga'], 'max_iter': [100, 500, 1000]}

# Use RandomizedSearchCV to find the best hyperparameters
random_search = RandomizedSearchCV(lr, param_distributions=param_dist, n_iter=10, cv=3)
random_search.fit(x_train, y_train)

# Print the best hyperparameters and accuracy score
print("Best hyperparameters: ", random_search.best_params_)
print("Best accuracy score: ", random_search.best_score_)

# Save the best trained model in .sav format
best_model = random_search.best_estimator_
#joblib.dump(best_model, 'logr_fs1.sav')

filename = f'logr_{feature_set}.sav'
pickle.dump(best_model, open(cwd + '\\Saved_Models\\logr\\' + filename, 'wb'))

# Load the saved model
#loaded_model = joblib.load('logr_fs1.sav')
loaded_model = pickle.load(open(filename, 'rb'))

# Make predictions on the test set and calculate accuracy
y_pred = loaded_model.predict(x_test)
accuracy = accuracy_score(y_test, y_pred)
print("Test set accuracy: ", accuracy)

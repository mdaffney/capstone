import os
import sys

from sklearn.metrics import accuracy_score
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import randint, uniform
import pickle

import warnings
warnings.filterwarnings("ignore")

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder


for feature_set in ['fs3', 'fs4', 'fs5']:

    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
    
    # Define the Gradient Boosting model
    gbt = GradientBoostingClassifier()
    
    # Define the hyperparameter distribution to search over
    param_dist = {'max_depth': randint(1, 10),'learning_rate': uniform(0.001, 0.1),'n_estimators': randint(50, 500) ,'subsample': uniform(0.5, 0.5),'max_features': ['auto', 'sqrt', 'log2'],'min_samples_split': randint(2, 10),'min_samples_leaf': randint(1, 5)}
    
    # Use RandomizedSearchCV to find the best hyperparameters
    random_search = RandomizedSearchCV(gbt, param_distributions=param_dist, n_iter=10, cv=5)
    random_search.fit(x_train, y_train)
    
    # Print the best hyperparameters and accuracy score
    print("Best hyperparameters: ", random_search.best_params_)
    print("Best accuracy score: ", random_search.best_score_)
    
    # Save the best trained model in .sav format
    best_model = random_search.best_estimator_
    # Save the best model
    
    file_name = f'gbt_{feature_set}.sav'
    with open(file_name, 'wb') as file:
        pickle.dump(best_model, file)
    
    # Load the saved model
    with open(file_name, 'rb') as file:
        loaded_model = pickle.load(file)
    
    
    # Make predictions on the test set and calculate accuracy
    y_pred = loaded_model.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    print("Test set accuracy: ", accuracy)

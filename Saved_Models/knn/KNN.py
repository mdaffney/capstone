# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 10:33:57 2023

@author: kents
"""
import sys
import os
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import accuracy_score
from scipy.stats import randint
import pickle

import warnings
warnings.filterwarnings("ignore")

# Determine relative starting path
absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

del ap_split, i

sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder


for feature_set in ['fs1', 'fs2', 'fs3', 'fs4', 'fs5']:
    # Get data
    fsb = FeatureSetBuilder()
    features, x, y = fsb.get(feature_set)
    
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=4)
    
    # Define the k-NN model
    knn = KNeighborsClassifier()
    
    # Define the hyperparameter distribution to search over
    param_dist = {'n_neighbors': randint(1, 20), 'weights': ['uniform', 'distance'],  'algorithm': ['ball_tree', 'kd_tree', 'brute'], 'p': [1, 2], 'leaf_size': randint(10,50)}
    
    # Use RandomizedSearchCV to find the best hyperparameters
    random_search = RandomizedSearchCV(knn, param_distributions=param_dist, n_iter=10, cv=3)
    random_search.fit(x_train, y_train)
    
    # Print the best hyperparameters and accuracy score
    print("Best hyperparameters: ", random_search.best_params_)
    print("Best accuracy score: ", random_search.best_score_)
    
    # Save the best trained model in .sav format
    best_model = random_search.best_estimator_
    # joblib.dump(best_model, open(cwd + '\\Saved_Models\\knn\\' + 'knn_fs1.sav', 'wb'))
    
    filename = f'knn_{feature_set}.sav'
    
    pickle.dump(best_model, open(cwd + '\\Saved_Models\\knn\\' + filename, 'wb'))
    # Load the saved model
    #loaded_model = joblib.load('knn_fs1.sav')
    
    loaded_model = pickle.load(open(filename, 'rb'))
    
    # Make predictions on the test set and calculate accuracy
    y_pred = loaded_model.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    print("Test set accuracy: ", accuracy)

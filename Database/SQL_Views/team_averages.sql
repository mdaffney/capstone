DROP VIEW IF EXISTS team_averages ;

CREATE VIEW team_averages

AS

SELECT 
	team_id,
	home_v_away,
	games.season,
	AVG(pts) as 'pts', 
	AVG(fgm) as 'fgm', 
	AVG(fga) as 'fga', 
	ROUND(SUM(fgm) / CAST(SUM(fga) as REAL), 4) as 'fgp', 
	AVG(ftm) as 'ftm', 
	AVG(fta) as 'fta', 
	ROUND(SUM(ftm) / CAST(SUM(fta) as REAL), 4) as 'ftp', 
	AVG(boxscores_team.'3fgm') as '3fgm', 
	AVG(boxscores_team.'3fga') as '3fga', 
	ROUND(SUM(boxscores_team.'3fgm') / CAST(SUM(boxscores_team.'3fga') as REAL), 4) as '3fgp', 
	AVG(offReb) as 'offReb', 
	AVG(defReb) as 'defReb', 
	AVG(assists) as 'assists', 
	AVG(pfouls) as 'pFouls', 
	AVG(steals) as 'steals', 
	AVG(turnovers) as 'turnovers', 
	AVG(blocks) as 'blocks' 

FROM games

LEFT JOIN boxscores_team on (boxscores_team.game_id = games.game_id)

-- games is missing game_id's that are in boxscores
WHERE boxscores_team.season IS NOT NULL

GROUP BY team_id, home_v_away, boxscores_team.season


DROP VIEW IF EXISTS model_accuracy_agg;

CREATE VIEW model_accuracy_agg
AS
SELECT
	--mp.game_id,
	max(mp.date),
	model,
	feature_set,
	SUM(prediction_away),
	SUM(prediction_home),
	--outcome,
	ROUND(SUM(CASE
		WHEN prediction_home = 1 and outcome = 'win'
			THEN 1
		WHEN prediction_home = 1 and outcome = 'loss'
			THEN 0
		WHEN prediction_home = 0 and outcome = 'loss'
			THEN 1
		WHEN prediction_home = 0 and outcome = 'win'
			THEN 0	
	END) / CAST(COUNT(prediction_home) AS REAL), 4) as 'pct_correct'

FROM 

model_predictions as mp

INNER JOIN games on (games.game_id = mp.game_id and games.status = 'Finished')

GROUP BY model, feature_set
DROP VIEW IF EXISTS model_accuracy;

CREATE VIEW model_accuracy
AS
SELECT
	mp.game_id,
	mp.date,
	model,
	feature_set,
	prediction_away,
	prediction_home,
	outcome,
	CASE
		WHEN prediction_home = 1 and outcome = 'win'
			THEN 1
		WHEN prediction_home = 1 and outcome = 'loss'
			THEN 0
		WHEN prediction_home = 0 and outcome = 'loss'
			THEN 1
		WHEN prediction_home = 0 and outcome = 'win'
			THEN 0	
	END as 'correct'

FROM 

model_predictions as mp

INNER JOIN games on (games.game_id = mp.game_id and games.status = 'Finished')
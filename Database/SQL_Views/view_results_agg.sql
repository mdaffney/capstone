DROP VIEW IF EXISTS results_agg;

CREATE VIEW results_agg
AS

SELECT
	op.date,
	--t1.name as 'away_team',
	--t2.name as 'home_team',
	--op.away_team_ml,
	--op.home_team_ml,
	--ROUND(op.away_team_implied_pct, 2) as 'away_imp_pct',
	--ROUND(op.home_team_implied_pct, 2) as 'home_imp_pct',
	op.model,
	op.feature_set,
	op.strategy,
	--op.decision,
	total_wager,
	SUM(op.ind_wager) as 'ind_wager',
	ROUND(SUM(op.payout), 2) as 'payout',
	--games.outcome,
	SUM (CASE
		WHEN decision = home_team_ml and outcome = 'win'
			THEN ROUND(op.payout, 2) 
		WHEN decision = home_team_ml and outcome = 'loss'
			THEN 0
		WHEN decision = away_team_ml and outcome = 'loss'
			THEN ROUND(op.payout, 2) 
		WHEN decision = away_team_ml and outcome = 'win'
			THEN 0
	END) as 'balance'
	
FROM optimizer_distributions as op

LEFT JOIN games on (games.game_id = op.game_id and games.date = op.date)
LEFT JOIN teams as t1 on (t1.team_id = games.away_team_id)
LEFT JOIN teams as t2 on (t2.team_id = games.home_team_id)

GROUP BY op.date, model, feature_set, strategy, total_wager
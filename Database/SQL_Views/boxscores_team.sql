DROP VIEW IF EXISTS boxscores_team ;

CREATE VIEW boxscores_team
AS
SELECT 
	boxscores.game_id, 
	team_id, 
	season,
	CASE WHEN boxscores.team_id = games.home_team_id THEN 
		'home'
	ELSE
		'away'
	END as 'home_v_away',
	ROUND(SUM(pts),4) as 'pts', 
	ROUND(SUM(fgm),4) as 'fgm', 
	ROUND(SUM(fga),4) as 'fga', 
	ROUND(SUM(fgm) / CAST(SUM(fga) as REAL), 4) as 'fgp', 
	ROUND(SUM(ftm),4) as 'ftm', 
	ROUND(SUM(fta),4) as 'fta', 
	ROUND(SUM(ftm) / CAST(SUM(fta) as REAL), 4) as 'ftp', 
	ROUND(SUM(boxscores.'3fgm'),4) as '3fgm', 
	ROUND(SUM(boxscores.'3fga'),4) as '3fga', 
	ROUND(SUM(boxscores.'3fgm') / CAST(SUM(boxscores.'3fga') as REAL), 4) as '3fgp', 
	ROUND(SUM(offReb),4) as 'offReb', 
	ROUND(SUM(defReb),4) as 'defReb', 
	ROUND(SUM(assists),4) as 'assists', 
	ROUND(SUM(pfouls),4) as 'pFouls', 
	ROUND(SUM(steals),4) as 'steals', 
	ROUND(SUM(turnovers),4) as 'turnovers', 
	ROUND(SUM(blocks),4) as 'blocks'

FROM boxscores

INNER JOIN games on (games.game_id = boxscores.game_id)

WHERE boxscores.'min' IS NOT NULL

GROUP BY boxscores.game_id, team_id

ORDER BY boxscores.game_id, home_v_away
DROP VIEW IF EXISTS player_averages ;

CREATE VIEW player_averages
AS
SELECT
	boxscores.player_id,
	games.season,
	AVG(min),
	AVG(fga),
	AVG(fgm),
	AVG(boxscores.'3fga'),
	AVG(boxscores.'3fgm'),
	AVG(ftm),
	AVG(defReb),
	AVG(totReb),
	AVG(turnovers),
	AVG(pFouls)

FROM
boxscores

LEFT JOIN games on (games.game_id = boxscores.game_id)

GROUP BY season, player_id
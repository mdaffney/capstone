# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 15:23:44 2023

@author: kents
"""

import os
import sqlite3
import requests
import pandas as pd
import time

from datetime import date
from datetime import datetime
from datetime import timedelta

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import MiscFeatures

class DatabaseManager:
    
    def __init__(self, database):
        self.api = r"https://api-nba-v1.p.rapidapi.com"
        
        self.headers = {
         	"X-RapidAPI-Key": "c767d9abcdmsh1452affa8db93e3p19ffb9jsncbc0c6648549",
         	"X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com"
             }
        
        absolute_path = os.path.dirname(__file__)

        ap_split = absolute_path.split('\\')
        i = ap_split.index('capstone')
        del ap_split[i + 1:]
        
        self.cwd = '\\'.join(ap_split)

        self.database = database
        
   
    def build_database(self):
        try:
            #TODO check if exists 
            conn = sqlite3.connect(self.cwd + "\\Database\\" + self.database)
            
            #Games table required for boxscore table
            print("Building games table...")
            self.build_games_table(conn, 2022)
            print("Building boxscores table...")
            self.build_box_score_table(conn)
            
            #Teams table required for players table
            print("Building teams table...")
            self.build_teams_table(conn)
            print("Building players table...")
            self.build_players_table(conn)
            print("Building odds table...")
            self.build_odds_table(conn)
            
            #TODO build views
        
        finally:
            conn.close()
    
    def update_database(self):
        try:
            conn = sqlite3.connect(self.cwd + "\\Database\\" + self.database)
            
            print('Updating database...')
            self.update_games_table_existing(conn)
            time.sleep(5)
            
            self.update_box_score_table(conn)
            time.sleep(5)
            
            self.update_games_by_team_table(conn)
            time.sleep(5)
            
            print('Updating the odds table:')
            self.build_odds_table(conn)
            print('Update complete.')
            
        finally:
            conn.close()
    
    def build_games_table(self, conn, season):
        endpoint = "/games"
        querystring = {"season": season}
        
        response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
        df = pd.json_normalize(response['response'])
        df = self.__games_table_formatting(df)

        df.to_sql('games', con = conn, if_exists='append')
        
        # May need if seasons not added in order
        #self.clean_table(conn, 'games')
    
    def add_playoff_games(self, conn, season, stage = 4):
        endpoint = "/games"
        querystring = {"season": season}
        
        response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
        
        df = pd.json_normalize(response['response'])
        df = self.__games_table_formatting(df, game_type = stage) # stage 3 - play in / stage 4 - playoffs

        if len(df) == 0:
            return

        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()
        game_ids = cur.execute("SELECT game_id FROM games").fetchall()
        
        missing_games = ~df.index.isin(game_ids)
    
        df = df.iloc[missing_games]

        conn.row_factory = None
        df.to_sql('games', con = conn, if_exists='append')
        
        # May need if seasons not added in order
        #self.clean_table(conn, 'games')
     
    def build_box_score_table(self, conn, limit = None):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("SELECT game_id FROM games where status = 'Finished'").fetchall()
        
        if limit is not None:
            game_ids = game_ids[:limit]
        
        print("Number of expected api calls is %s" % len(game_ids))
        user_input = input("Would you like to continue (y/n):\n")
        
        if user_input != 'y':
            return
        
        #TODO reset this cache when building
        f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'r')
        bad_ids = [int(line) for line in f.readlines()]
        f.close()

        endpoint = "/players/statistics"

        for game_id in game_ids:
            if game_id in bad_ids:
                continue
            
            querystring = {"game": str(game_id)}
            
            response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
            df = pd.json_normalize(response['response'])
            df = self.__box_score_table_formatting(df)
            
            # Verify request game_id equals returned id
            if game_id != df.index.values[0]:
                #Add to bad ids
                f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'a')
                f.write(str(game_id) + '\n')
                f.close()
                continue

            df.to_sql('boxscores', con = conn, if_exists='append')
            
        self.clean_table(conn, 'boxscores')
            
    
    def build_teams_table(self, conn):
        endpoint = "/teams"

        response = requests.request("GET", self.api + endpoint, headers = self.headers).json()

        df = pd.json_normalize(response['response'])
        df = df.query("nbaFranchise == True and allStar == False")

        df = df.iloc[:, 0:10]

        columns_to_keep = [0, 1, 2, 3, 4, 8, 9]      
        df = df.iloc[:, columns_to_keep].copy()

        column_remames = {'id': 'team_id',
                          'leagues.standard.conference': 'conference', 
                          'leagues.standard.division': 'division'
                          }

        df = df.rename(columns = column_remames)

        df = df.set_index('team_id')
        df = df.sort_index()

        df.to_sql('teams', con = conn, if_exists='replace')
        
        # Shouldn't need...
        #self.clean_table(conn, 'teams')
    
    
    def build_players_table(self, conn, season):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()
        team_ids = cur.execute("SELECT team_id FROM teams").fetchall()
    
        endpoint = "/players"
    
        for team_id in team_ids:
    
            querystring = {"team": str(team_id), "season": str(season)}
            response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
    
            df = pd.json_normalize(response['response'])
            
            #temp = df.copy()
    
            #df = df.iloc[:, 0:17].copy()
            
            df = df[['id', 'firstname', 'lastname', 'college', 'affiliation', 'birth.date',
                   'birth.country', 'nba.start', 'nba.pro', 'height.feets',
                   'height.inches', 'height.meters', 'weight.pounds', 'weight.kilograms',
                   'leagues.standard.jersey', 'leagues.standard.active',
                   'leagues.standard.pos']]
    
            
            column_remames = {'id': 'player_id'}
            df = df.rename(columns = column_remames)
            
            df = df.set_index('player_id')
    
            df.to_sql('players', con = conn, if_exists='append')
         
        self.clean_table(conn, 'players')
    
    
    def build_odds_table(self, conn):
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

        try:
            driver.get("https://sports.yahoo.com/nba/odds/")  
            
            #Give site a change to load
            time.sleep(2)
    
            matchups = driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div')
            ind_matchups = matchups.find_elements('xpath', './child::*')
    
            data = {'day': [],
                    'date': [],
                    'time': [],
                    'away_team': [],
                    'away_team_ml': [],
                    'home_team': [],
                    'home_team_ml': []}
    
    
            for i in range(1, len(ind_matchups) + 1):
                full_date = driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div/div[%s]/div[1]/div/table/thead/tr/th[1]/span/span/span' % i).text
                
                try:
                    day, short_date, game_time = full_date.strip(' ').split(',')
                except:
                    continue
                
                
                #TODO change to yesterday's year
                date_str = short_date + '/' + str(str(date.today().year))
                date_object = datetime.strptime(date_str, ' %m/%d/%Y').date()
                if date_object != date.today():
                    continue
                
                data['day'].append(day)
                data['date'].append(date_object.strftime('%Y-%m-%d'))
                data['time'].append(game_time)
                    
                    
                data['away_team'].append(
                    driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div/div[%s]/div[1]/div/table/tbody/tr[1]/td[1]/div/span[1]' % i).text
                    )
    
                data['away_team_ml'].append(
                    driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div/div[%s]/div[1]/div/table/tbody/tr[1]/td[2]/div/div/button/span/span' % i).text
                    )
    
                data['home_team'].append(
                    driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div/div[%s]/div[1]/div/table/tbody/tr[2]/td[1]/div/span[1]' % i).text
                    )
                
                data['home_team_ml'].append(
                    driver.find_element('xpath', '//*[@id="Col1-1-LeagueOdds-Proxy"]/div/div[5]/div/div/div[%s]/div[1]/div/table/tbody/tr[2]/td[2]/div/div/button/span/span' % i).text
                    )
        finally: 
            driver.close()

        df = pd.DataFrame(data)
        df = df.set_index(['day', 'date', 'time', 'away_team', 'home_team'])
        df.to_sql('odds', con = conn, if_exists='append')

#TODO need to test
    def update_games_table_existing(self, conn):
        yesterday = date.today() - timedelta(days = 1)

        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("SELECT game_id FROM games where (season = 2022 and status = 'Scheduled' and  home_team_pts IS NULL and date <= '%s')" % (yesterday.strftime('%Y-%m-%d'))).fetchall()

        endpoint = "/games"

        print("Updating the following games in the Games table:")
        for game_id in game_ids:
            querystring = {"id":"%s" % game_id}

            #TODO check if bad id

            response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
            df = pd.json_normalize(response['response'])
            
            if len(response["response"]) == 0:
                print(f"\t{game_id}: API missing data")
                continue
            
            
            if df['scores.home.points'][0] is None or df['scores.visitors.points'][0] is None:
                f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'a')
                f.write(str(game_id) + '\n')
                f.close()
                continue

            df = self.__games_table_formatting(df, game_type=None)
  
            sql_update_stmt = """
                UPDATE games
                SET
                    home_team_pts = %d,
                    away_team_pts = %d,
                    status = '%s',
                    outcome = '%s'
                WHERE
                    game_id = %d
                """ % (df['home_team_pts'].values[0], df['away_team_pts'].values[0], df['status'].values[0], df['outcome'].values[0], df.index.values[0])
            
            print("\t" + str(game_id))
            cur.execute(sql_update_stmt)
        conn.commit()
    
    
    def check_if_games_today(self, conn):
        today_str = date.today().strftime('%Y-%m-%d')

        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("""SELECT date from games""")
        
        if today_str in game_ids:
            return True
        
        #print("Checking for playoff, play in tournament games...")
        self.add_playoff_games(conn, 2022, stage = 3)

        game_ids = cur.execute("""SELECT date from games""")
        if today_str in game_ids:
            return True
        
        #print("Checking for playoff games...")
        self.add_playoff_games(conn, 2022)
        
        game_ids = cur.execute("""SELECT date from games""")
        if today_str in game_ids:
            return True
        
        #print("No games today.")
        return False
        
    
    def update_games_table_missing(self, conn):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("SELECT game_id FROM games where status = 'Finished'").fetchall()
        boxscore_game_ids = cur.execute("SELECT game_id FROM boxscores").fetchall()

        missing_game_ids = set(boxscore_game_ids).difference(set(game_ids))
        
        if len(missing_game_ids) == 0:
            print('Games table already up to date.')
            return
             
        endpoint = "/games"

        for game_id in sorted(missing_game_ids):
            
            querystring = {"id": str(game_id)}
            
            response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()

            df = pd.json_normalize(response['response'])
            df = self.__games_table_formatting(df)
            
            df.to_sql('games', con = conn, if_exists='append')

    def print_diff(self, conn):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("SELECT game_id FROM games where status = 'Finished'").fetchall()
        boxscore_game_ids = cur.execute("SELECT game_id FROM boxscores").fetchall()

        missing_game_ids = set(boxscore_game_ids).difference(set(game_ids))
        new_game_ids = set(game_ids).difference(set(boxscore_game_ids))
        
        print(sorted(missing_game_ids))
        print(sorted(new_game_ids))
        
#TODO need to test
    def update_box_score_table(self, conn, limit = None):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        game_ids = cur.execute("SELECT game_id FROM games where status = 'Finished'").fetchall()
        boxscore_game_ids = cur.execute("SELECT game_id FROM boxscores").fetchall()

        new_game_ids = set(game_ids).difference(set(boxscore_game_ids))
        if len(new_game_ids) == 0:
            print('Boxscore table already up to date.')
            return
        if limit is not None:
            new_game_ids = list(new_game_ids)[:limit]

        # Known bad game ids...
        f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'r')
        bad_ids = [int(line) for line in f.readlines()]
        f.close()

        # TODO x = new_game_ids.diff(bad_ids)

        # For each game id get boxscore data
        endpoint = "/players/statistics"

        print("Updating the following games in the Boxscores table:")
        no_requests = 0
        for game_id in sorted(new_game_ids):
            if game_id in bad_ids:
                continue
            
            querystring = {"game": str(game_id)}
            
            response = requests.request("GET", self.api + endpoint, headers = self.headers, params = querystring).json()
            no_requests += 1
            
            # No response
            if response['results'] == 0:
                print(f"\t{game_id}: API missing data")
                #Add to bad ids
                f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'a')
                f.write(str(game_id) + '\n')
                f.close()
                continue
            
            df = pd.json_normalize(response['response'])
            df = self.__box_score_table_formatting(df)
            
            # Verify request game_id equals returned id
            if game_id != df.index.values[0][0]:
                #Add to bad ids
                f = open(self.cwd + "\\Database\\db_cache_bad_game_ids.txt", 'a')
                f.write(str(game_id) + '\n')
                f.close()
                continue
            
            
            df.to_sql('boxscores', con = conn, if_exists='append')
            
            print("\t" + str(game_id))
        #print(no_requests)

    def update_games_by_team_table(self, conn):
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()
    
        game_ids = cur.execute("SELECT game_id FROM games where status = 'Finished'").fetchall()
        game_ids_by_team = cur.execute("SELECT DISTINCT game_id FROM games_by_team").fetchall()
        new_game_ids = set(game_ids).difference(set(game_ids_by_team))
        
        if len(new_game_ids) == 0:
            print('Games_by_team table already up to date.')
            return
        
        conn.row_factory = None
        games_by_team = pd.read_sql("SELECT * FROM games_by_team", conn)
        games_by_team['date'] = pd.to_datetime(games_by_team['date'], format = '%Y-%m-%d')
        stadiums = pd.read_sql("SELECT * FROM stadiums", conn)
        
        query = open(self.cwd + "//Saved_Feature_Sets//FS5//" + "query_fs5_games_by_team_update.sql", 'r').read()
        
        for game in new_game_ids:
            df = pd.read_sql(query, conn, params = [game, game])
            
            df['date'] = pd.to_datetime(df['date'], format = '%Y-%m-%d')
            
            df['days_since_last_game'] = df.apply(lambda x: MiscFeatures.days_since_last_game(games_by_team, x.team_id, x.date, x.season), axis = 1)
            
            df['lat_long'] = df.apply(lambda x: MiscFeatures.location_played(stadiums, x.home_away, x.team_id, x.opponent_id), axis = 1)
            df[['lat', 'long']] = pd.DataFrame(df['lat_long'].tolist(), index = df.index)
            df = df.drop(columns = ['lat_long'])
            
            df['dist_traveled'] = df.apply(lambda x: MiscFeatures.distance_from_last_game(games_by_team, x.team_id, x.date, x.season, x.lat, x.long), axis = 1)
                   
            df['ELO_before'] = df.apply(lambda x: 1500, axis = 1)
            df['ELO_after'] = df.apply(lambda x: 1500, axis = 1)
            for i, row in df.iterrows():
                elo_b, elo_a = MiscFeatures.elo_calculation(games_by_team, row)
                df.loc[i, 'ELO_before'] = elo_b
                df.loc[i, 'ELO_after'] = elo_a
            
            df = df.set_index(['game_id', 'team_id'])
            df.to_sql('games_by_team', con = conn, if_exists='append')

    def build_views(self, conn):
        cur = conn.cursor()
        
        directory = self.cwd + "\\Database\\SQL_Views"

        for filename in os.listdir(directory):
            f = os.path.join(directory, filename)

            if os.path.isfile(f):
                cur.executescript(open(f).read())
        
        conn.commit()
        
        
        
    def clean_table(self, conn, table):
        cur = conn.cursor()
        
        if table == 'games':
            parameters = 'game_id ASC'
            
        elif table == 'boxscores':
            parameters = "game_id ASC, team_id ASC, 'min' DESC"
            
        elif table == 'teams':
            parameters = 'team_id ASC'
            
        elif table == 'players':
            parameters = 'player_id ASC'
        
        else:
            #TODO throw error
            return
        
        cur.execute("""ALTER TABLE %s RENAME TO temp;""" % table)
        cur.execute("""CREATE TABLE %s
                    AS
                    SELECT * 
                    FROM temp
                    ORDER BY %s;""" % (table, parameters))
        
        cur.execute( """DROP TABLE temp;""")
        conn.commit()
        
        #Recreate views if games/boxscores cleaned... views continue to reference altered and dropped table
        if table == 'games' or table == 'boxscores':
            self.build_views(conn)
    
    def delete_duplicate_rows(self, conn, table):
        cur = conn.cursor()
        
        if table == 'odds':
            columns = "day, date, time, away_team, home_team, away_team_ml, home_team_ml"
        elif table == 'boxscores':
            #TODO way to get with out specifying...
            columns = "game_id, team_id, player_id, pts, min, fgm, fga, fgp, ftm, fta, ftp, boxscores.'3fgm', boxscores.'3fga', boxscores.'3fgp', offReb, defReb, totReb, assists, pFouls, steals, turnovers, blocks, plusMinus"
        elif table == 'players':
            columns = "player_id, firstname, lastname, college, affiliation, 'birth.date', 'birth.country', 'nba.start', 'nba.pro', 'height.feets', 'height.inches', 'height.meters', 'weight.pounds', 'weight.kilograms', 'leagues.standard.jersey', 'leagues.standard.active', 'leagues.standard.pos'"
        elif table == 'games':
            columns = "game_id, season, date, home_team_id, away_team_id, home_team_pts, away_team_pts, status, outcome"
        
        else:
            return
        
        cur.execute("""
                    DELETE 
                    FROM %s
                    WHERE ROWID NOT IN
                     (
                     SELECT min(ROWID)
                     FROM %s
                     GROUP BY %s
                     )
                    """ % (table, table, columns))
        conn.commit()
    
    # Could be more extensive... check boxscores and odds tables
    def isUpToDate(self):
        
   
        conn = sqlite3.connect(self.cwd + "\\Database\\" + self.database)
        cur = conn.cursor()
        
        # Check if games table needs to update past games
        games_results = cur.execute("""SELECT * 
                                 FROM games
                                 WHERE date < '%s' and status = 'Scheduled'""" % date.today().strftime('%Y-%m-%d')).fetchall()
        
        gamesToday = self.check_if_games_today(conn)
            
        # Check if odds table has updated with today's games
        odds_results = cur.execute("""SELECT * 
                                 FROM odds
                                 WHERE date == '%s' """ % date.today().strftime('%Y-%m-%d')).fetchall()
        

        if len(games_results) != 0:
            print("Database needs to be updated.")
            return False
        elif gamesToday and len(odds_results) == 0:
            print("Database needs to be updated.")
            return False
        else:
            print("Database is up-to-date.")
            return True
        
        conn.close()
        
    # Private Methods -----------------------------------------------------------------------------------------------------------
    def __games_table_formatting(self, df, game_type = 2):
        # Stage 2  --> Regular Season
        if game_type is None:
            df = df.query("league == 'standard'")
        else:
            df = df.query(f"league == 'standard' and stage == {game_type}")

        if (len(df) == 0):
            return df

        columns_to_keep = [0, 2, 8, 27, 22, 43, 37, 14]      
        df = df.iloc[:, columns_to_keep].copy()

        column_remames = {'id': 'game_id', #0
                          'date.start': 'date', 
                          'teams.home.id': 'home_team_id', #27
                          'teams.visitors.id': 'away_team_id', #22
                          'scores.home.points': 'home_team_pts', #43
                          'scores.visitors.points': 'away_team_pts', #37
                          'status.long': 'status' #14
                          }

        df = df.rename(columns = column_remames)
        del columns_to_keep

        df = df.set_index('game_id')
        df = df.sort_index()

        df['date'] = pd.to_datetime(df['date'], infer_datetime_format = True, utc = True)
        df['date'] = df['date'].dt.tz_convert('US/Eastern')

        df['date'] = df['date'].dt.strftime('%Y-%m-%d')

        # df['outcome'] = None
        # for index, game in df.iterrows():   
        #     game['outcome'] = 'win' if game['home_team_pts'] > game['away_team_pts'] else 'loss'
            
        df['outcome'] = df.apply(lambda x: 'win' if x.home_team_pts > x.away_team_pts else 'loss', axis=1)
        
        return df
    
        
    def __box_score_table_formatting(self, df):
        columns_to_keep = [30, 25, 22, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]      
        df = df.iloc[:, columns_to_keep].copy()

        column_remames = {'points': 'pts', #0
                          'tpm': '3fgm', #9
                          'tpa': '3fga', #10
                          'tpp': '3fgp', #11
                          'player.id': 'player_id', #22
                          'team.id': 'team_id', #25
                          'game.id': 'game_id' #30
                          }

        df = df.rename(columns = column_remames)
        del columns_to_keep, column_remames
        
        df = df.set_index(['game_id', 'team_id', 'player_id'])
        
        return df

        column_remames = {'points': 'pts', #0
                          'tpm': '3fgm', #9
                          'tpa': '3fga', #10
                          'tpp': '3fgp', #11
                          'player.id': 'player_id', #22
                          'team.id': 'team_id', #25
                          'game.id': 'game_id' #30
                          }

        df = df.rename(columns = column_remames)
        df = df.set_index(['game_id', 'team_id', 'player_id'])
        
        return df
    
    
if __name__ == "__main__":
    
    #Miscellaneous Manual Maintenance
    # db = DatabaseManager('v1_database.db')
    
    # try:
    #     conn = sqlite3.connect(db.database)
        
    #     #db.build_games_table(conn, 2022)
    #     #db.clean_table(conn, 'games')
    #     #db.update_box_score_table(conn)
    #     #db.delete_duplicate_rows(conn, 'games')
    #     db.add_playoff_games(conn, '2022', stage=3)
    
    # except Exception as err:
    #     print(f"Unexpected {err=}, {type(err)=}")
        
    # finally:
    #     conn.close()
    
    
    # Daily Update ------------------------------------------------------------
    
    db = DatabaseManager('v1_database.db')
    
    try:
        
        if not db.isUpToDate():
            db.update_database()
            
    except Exception as err:
        print(type(err))
        print(err)
        
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 14:15:22 2023

@author: kents
"""
import geopy.distance


def days_since_last_game(games_by_team, team_id, date, season):
       
    filter1 = games_by_team['team_id'] == team_id
    filter2 = games_by_team['date'] < date
    filter3 = games_by_team['season'] == season
    
    prev_game = games_by_team.where(filter1 & filter2 & filter3).dropna().tail(1)
    
    if len(prev_game['date'].values) != 0:
        return (date - prev_game['date'].values[0]).days
    else:
        return float('inf')

def location_played(stadiums, h_or_a, team_id, opponent_id):
    if h_or_a == 'home':
        lat = stadiums.query("team_id == %s" % (team_id))['latitude'].values[0]
        long = stadiums.query("team_id == %s" % (team_id))['longitude'].values[0]
    
    else:
        lat = stadiums.query("team_id == %s" % (opponent_id))['latitude'].values[0]
        long = stadiums.query("team_id == %s" % (opponent_id))['longitude'].values[0]
        
    return lat, long

def distance(coords_1, coords_2):
    return geopy.distance.geodesic(coords_1, coords_2).km
    

def distance_from_last_game(games_by_team, team_id, date, season, lat, long):
    
    filter1 = games_by_team['team_id'] == team_id
    filter2 = games_by_team['date'] < date
    filter3 = games_by_team['season'] == season
    
    prev_game = games_by_team.where(filter1 & filter2 & filter3).dropna().tail(1)

    if len(prev_game['date'].values) != 0:
        return distance((lat, long), (prev_game['lat'].values[0], prev_game['long'].values[0]))
    else:
        return 0

def calc_k(mov, elo_diff):
    
    nominator = (mov + 3)**0.8
    denominator = 7.5 + 0.006 * elo_diff
    
    return 20 * (nominator / denominator)

def previous_season_elo_calc(games_by_team, team_id, season):
    
    filter1 = games_by_team['team_id'] == team_id
    filter2 = games_by_team['season'] == season - 1
    
    prev_game = games_by_team.where(filter1 & filter2).dropna().tail(1)
    
    if len(prev_game['date'].values) != 0:
        return (prev_game['ELO_after'].values[0] * 0.75) + (0.25 * 1505)
    else:
        return 1500

def previous_elo(games_by_team, df):
    
    filter1 = games_by_team['team_id'] == df.team_id
    filter3 = games_by_team['season'] == df.season
    filter2 = games_by_team['date'] < df.date
    
    prev_game = games_by_team.where(filter1 & filter2 & filter3).dropna().tail(1)
    
    if len(prev_game['date'].values) == 0:
        return previous_season_elo_calc(games_by_team, df.team_id, df.season)
    else:
        return prev_game['ELO_after'].values[0]


def elo_calculation(games_by_team, df):
    
    # Team's Previous Game ------------------------------------------------------------------------
    filter1 = games_by_team['team_id'] == df.team_id
    filter3 = games_by_team['season'] == df.season
    filter2 = games_by_team['date'] < df.date
    
    prev_game = games_by_team.where(filter1 & filter2 & filter3).dropna().tail(1)
    
    if len(prev_game['date'].values) == 0:
        ELO_current = previous_season_elo_calc(games_by_team, df.team_id, df.season)
    else:
        ELO_current = prev_game['ELO_after'].values[0]
    
    ELO_original = ELO_current
   
    # Team's Previous Game ------------------------------------------------------------------------
    filter1 = games_by_team['team_id'] == df.opponent_id
    filter3 = games_by_team['season'] == df.season
    filter2 = games_by_team['date'] < df.date
    
    prev_game_opp = games_by_team.where(filter1 & filter2 & filter3).dropna().tail(1)
    
    if len(prev_game_opp['date'].values) == 0:
        ELO_current_opp = previous_season_elo_calc(games_by_team, df.opponent_id, df.season)
    else:
        ELO_current_opp = prev_game_opp['ELO_after'].values[0]
    
    
    # Calculation ---------------------------------------------------------------------------------
    
    # Win Loss State
    S = 1 if df.outcome == 'win' else 0
    
    # Home Court Advantage
    if df.home_away == 'home':
        ELO_current += 100
    else:
        ELO_current_opp += 100
    
    # Probablities
    Q = 10 ** (ELO_current / 400)
    Q_opp = 10 ** (ELO_current_opp / 400)
    
    E = Q / (Q + Q_opp)
    
    # k const
    k = 0
    if ELO_current > ELO_current_opp and df.outcome == "win":
        k = calc_k(abs(df.pts - df.opponent_pts), ELO_current - ELO_current_opp)
        
    elif ELO_current > ELO_current_opp and df.outcome == "loss":
        k = calc_k(abs(df.pts - df.opponent_pts), ELO_current_opp - ELO_current)
        
    elif ELO_current < ELO_current_opp and df.outcome == "win":
        k = calc_k(abs(df.pts - df.opponent_pts), ELO_current - ELO_current_opp)
        
    else:
        k = calc_k(abs(df.pts - df.opponent_pts), ELO_current_opp - ELO_current)
    
    return ELO_original, ELO_original + k * (S - E)
    
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 11:54:53 2023

@author: kents
"""

import os
import sqlite3
import pandas as pd
import datetime
import numpy as np

import MiscFeatures

class FeatureSetBuilder:
    
    def __init__(self, date = datetime.date.today().strftime('%Y-%m-%d')):
        
        self.date = date
        
        self.Saved_Feature_Sets = ['fs1', 'fs1r', 'fs2', 'fs3', 'fs4', 'fs5']
        
        absolute_path = os.path.dirname(__file__)

        ap_split = absolute_path.split('\\')
        i = ap_split.index('capstone')
        del ap_split[i + 1:]
        
        self.cwd = '\\'.join(ap_split)
        
        self.database = 'v1_database.db'

    
    def get(self, feature_set):
        if feature_set not in self.Saved_Feature_Sets:
            raise ValueError()

        if feature_set == 'fs1':
            return self.__get_fs1()
        
        elif feature_set == 'fs1r':
            return self.__get_fs1r()
 
        elif feature_set == 'fs2':
            return self.__get_fs2()
        
        elif feature_set == 'fs3':
            return self.__get_fs3()
                
        elif feature_set == 'fs4':
            return self.__get_fs4()
        
        elif feature_set == 'fs5':
            return self.__get_fs5()
        
        
    def get_prediction_set(self, feature_set):
        if feature_set not in self.Saved_Feature_Sets:
            raise ValueError()
    
        if feature_set == 'fs1':
            return self.__get_fs1_prediction_set(self.date, self.date)
        
        elif feature_set == 'fs1r':
            return self.__get_fs1r_prediction_set(self.date, self.date)
        
        elif feature_set == 'fs2':
            return self.__get_fs2_prediction_set(self.date, self.date)
        
        elif feature_set == 'fs3':
            return self.__get_fs3_prediction_set(self.date, self.date)
        
        elif feature_set == 'fs4':
            return self.__get_fs4_prediction_set(self.date, self.date)
        
        elif feature_set == 'fs5':
            return self.__get_fs5_prediction_set(self.date, self.date)
        
        
    # Feature Set 1 ------------------------------------------------------------
    def __get_fs1(self):
        """Returns training and testing sets for Feature Set 1"""
        
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS1\\' + 'fs1.pickle')
        
        # Format data
        x = df.drop(['game_id', 'date', 'away_team_id', 'home_team_id', 'outcome'], axis = 1)
        y = df['outcome'].replace({'loss': 0, 'win': 1})
        
        features = list(x.columns)
        
        return features, np.array(x), np.array(y) 
    
    def __get_fs1_prediction_set(self, start_date, end_date):
        
        with open(self.cwd + '\\Saved_Feature_Sets\\FS1\\' + 'query_fs1_prediction.sql', 'r') as file:
            prediction_query = file.read()

        conn = sqlite3.connect(self.cwd + '\\Database\\' + self.database)
        prediction = pd.read_sql(prediction_query, conn, params = [start_date, end_date])
        conn.close()

        index = prediction[['game_id', 'date', 'away_team_id', 'home_team_id']]
        x = prediction.drop(['game_id', 'date', 'away_team_id', 'home_team_id'], axis = 1)
        
        return index, np.array(x)
    
    # Feature Set 1 Reduced ----------------------------------------------------
    def __get_fs1r(self):
        """Returns training and testing sets for Feature Set 1"""
        
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS1\\' + 'fs1.pickle')
        
        df['away_totReb'] = df['away_offReb'] + df['away_defReb']
        df['home_totReb'] = df['home_offReb'] + df['home_defReb']
        
        # Format data
        x = df[['away_fga', 'away_fgp', 'away_3fgp', 'away_ftm', 'away_defReb', 'away_totReb', 'away_turnovers', 'away_pFouls',
                'home_fga', 'home_fgp', 'home_3fgp', 'home_ftm', 'home_defReb', 'home_totReb', 'home_turnovers', 'home_pFouls'
                ]]
        
        
        y = df['outcome'].replace({'loss': 0, 'win': 1})
        
        features = list(x.columns)
        
        return features, np.array(x), np.array(y) 
    
    def __get_fs1r_prediction_set(self, start_date, end_date):
        prediction_query = open(self.cwd + '\\Saved_Feature_Sets\\FS1\\' + 'query_fs1_prediction.sql', 'r').read()

        conn = sqlite3.connect(self.cwd + '\\Database\\' + self.database)
        prediction = pd.read_sql(prediction_query, conn, params = [start_date, end_date])
        conn.close()
        
        prediction['away_totReb'] = prediction['away_offReb'] + prediction['away_defReb']
        prediction['home_totReb'] = prediction['home_offReb'] + prediction['home_defReb']

        index = prediction[['game_id', 'date', 'away_team_id', 'home_team_id']]
        
        x = prediction[['away_fga', 'away_fgp', 'away_3fgp', 'away_ftm', 'away_defReb', 'away_totReb', 'away_turnovers', 'away_pFouls',
                'home_fga', 'home_fgp', 'home_3fgp', 'home_ftm', 'home_defReb', 'home_totReb', 'home_turnovers', 'home_pFouls'
                ]]
              
        return index, np.array(x)
    
    # Feature Set 2 ------------------------------------------------------------
    def __get_fs2(self):
        
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS2\\' + 'fs2.pickle')
        
        x = df.drop(df.columns[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 28, 29, 30]], axis = 1)
        
        x.columns =  ['away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                      'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                      'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                      'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks']
        
        
        y = df[df.columns[5]].replace({'loss': 0, 'win': 1})
        y.columns = ['outcome']

        features = list(x.columns)
        
        return features, np.array(x), np.array(y)
            

    def __get_fs2_prediction_set(self, start_date, end_date):
        
        with open(self.cwd + '\\Saved_Feature_Sets\\FS2\\' + 'query_fs2_prediction.sql', 'r') as q1:
            prediction_query = q1.read()
        
        with open(self.cwd + '\\Saved_Feature_Sets\\FS2\\' + 'query_fs2_rolling_average.sql', 'r') as q2:
            rolling_avg = q2.read()

        conn = sqlite3.connect(self.cwd + '\\Database\\' + self.database)
        cur = conn.cursor()
        
        games = cur.execute(prediction_query, [start_date, end_date]).fetchall()
        
        away_list = []
        home_list = []

        for i, (game_id, season, date, away_team_id, home_team_id, outcome) in enumerate(games):
            
            away = cur.execute(rolling_avg, [away_team_id, "away", season, date]).fetchone()
            home = cur.execute(rolling_avg, [home_team_id, "home", season, date]).fetchone()
            
            # Accounts for the beginning of seasons
            if away is None:
                away = (season, date, away_team_id, "away",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                
            if home is None:
                home = (season, date, home_team_id, "home",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                
            away_list.append(away)
            home_list.append(home)
            
        conn.close()
            
        df_games = pd.DataFrame(games)
        df_away = pd.DataFrame(away_list)
        df_home = pd.DataFrame(home_list)
        df_final = pd.concat([df_games, df_away, df_home], axis = 1)
        df_final.columns = range(df_final.columns.size)
        
        if(len(df_final) == 0):
            return pd.DataFrame(), pd.DataFrame()
        
        df_final = df_final.drop(df_final.columns[[1, 5, 6, 7, 8, 9, 27, 28, 29, 30]], axis = 1)

        df_final.columns =  ['game_id', 'date', 'away_team_id', 'home_team_id',
                             'away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                             'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                             'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                             'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks']
        
        index = df_final[['game_id', 'date', 'away_team_id', 'home_team_id']]
        x = df_final.drop(['game_id', 'date', 'away_team_id', 'home_team_id'], axis = 1)
        
        return index, np.array(x)


    # Feature Set 3 ------------------------------------------------------------
    def __get_fs3(self):
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS3\\' + 'fs3_10_10.pickle')
        
        x = df.drop(df.columns[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 27, 28, 29, 30]], axis = 1)
        
        x.columns =  ['away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                      'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                      'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                      'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks']
        
        
        y = df[df.columns[5]].replace({'loss': 0, 'win': 1})
        y.columns = ['outcome']

        features = list(x.columns)
        
        return features, np.array(x), np.array(y)
    
    def __get_fs3_prediction_set(self, start_date, end_date):
        
        with open(self.cwd + '\\Saved_Feature_Sets\\FS3\\' + 'query_fs3_prediction.sql', 'r') as q1:
            prediction_query = q1.read()
        
        with open(self.cwd + '\\Saved_Feature_Sets\\FS3\\' + 'query_fs3_rolling_average.sql', 'r') as q2:
            rolling_avg = q2.read()
        
        conn = sqlite3.connect(self.cwd + '\\Database\\' + self.database)
        cur = conn.cursor()
        
        games = cur.execute(prediction_query, [start_date, end_date]).fetchall()
        
        away_list = []
        home_list = []

        limit_away = 5
        limit_home = 5

        for i, (game_id, season, date, away_team_id, home_team_id, outcome) in enumerate(games):
            
            away = cur.execute(rolling_avg, [away_team_id, "away", season, date, limit_away]).fetchone()
            home = cur.execute(rolling_avg, [home_team_id, "home", season, date, limit_home]).fetchone()
            
            # Accounts for the beginning of seasons
            if away is None:
                away = (season, date, away_team_id, "away",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                
            if home is None:
                home = (season, date, home_team_id, "home",	0, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                
            away_list.append(away)
            home_list.append(home)
            
        conn.close()
            
        df_games = pd.DataFrame(games)
        df_away = pd.DataFrame(away_list)
        df_home = pd.DataFrame(home_list)
        df_final = pd.concat([df_games, df_away, df_home], axis = 1)
        df_final.columns = range(df_final.columns.size)
        
        if(len(df_final) == 0):
            return pd.DataFrame(), pd.DataFrame()
        
        df_final = df_final.drop(df_final.columns[[1, 5, 6, 7, 8, 9, 27, 28, 29, 30]], axis = 1)

        df_final.columns =  ['game_id', 'date', 'away_team_id', 'home_team_id',
                             'away_pts', 'away_fgm', 'away_fga', 'away_fgp', 'away_ftm', 'away_fta', 'away_ftp', 'away_3fgm', 'away_3fga', 'away_fg3p', 'away_offReb', 
                             'away_defReb', 'away_assists', 'away_pFouls', 'away_steals', 'away_turnovers', 'away_blocks', 
                             'home_pts', 'home_fgm', 'home_fga', 'home_fgp', 'home_ftm', 'home_fta', 'home_ftp', 'home_3fgm', 'home_3fga', 'home_fg3p', 'home_offReb', 
                             'home_defReb', 'home_assists', 'home_pFouls', 'home_steals', 'home_turnovers', 'home_blocks']
        
        index = df_final[['game_id', 'date', 'away_team_id', 'home_team_id']]
        x = df_final.drop(['game_id', 'date', 'away_team_id', 'home_team_id'], axis = 1)
        
        return index, np.array(x)
    
    # Feature Set 4 ------------------------------------------------------------
    def __get_fs4(self):
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS4\\' + 'fs4_10_10.pickle')
        
        x = df.drop(df.columns[[0, 1, 2, 3, 4, 5]], axis = 1)
        y = df[df.columns[5]].replace({'loss': 0, 'win': 1})

        features = list(x.columns)
        
        return features, np.array(x), np.array(y)
    
    
    def __get_fs4_prediction_set(self, start_date, end_date):
        index_fs2, x_fs2 = self.__get_fs2_prediction_set(start_date, end_date)
        index_fs3, x_fs3 = self.__get_fs3_prediction_set(start_date, end_date)
        
     
        if len(index_fs2) == 0 or len(index_fs3) == 0:
            return pd.DataFrame(), pd.DataFrame()
        
        assert(index_fs2.equals(index_fs3))
        
        return index_fs2, np.hstack((x_fs2, x_fs3))
    
    # Feature Set 5 ------------------------------------------------------------
    def __get_fs5(self):
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS5\\' + 'fs5.pickle')
        
        x = df.drop(df.columns[[0, 1, 2, 3, 4, 5]], axis = 1)
        y = df[df.columns[5]].replace({'loss': 0, 'win': 1})

        features = list(x.columns)
        
        return features, np.array(x), np.array(y)
    
    
    def __get_fs5_prediction_set(self, start_date, end_date):
        index, values = self.__get_fs2_prediction_set(start_date, end_date)
        
        with sqlite3.connect(self.cwd + '\\Database\\' + self.database) as conn:
            games_by_team = pd.read_sql("SELECT * FROM games_by_team", conn)
            games_by_team['date'] = pd.to_datetime(games_by_team['date'], format = '%Y-%m-%d')
            stadiums = pd.read_sql("SELECT * FROM stadiums", conn)
            
            with open(self.cwd + "//Saved_Feature_Sets//FS5//" + "query_fs5_games_by_team_prediction.sql", 'r') as q1:
                query = q1.read()
                
            df = pd.read_sql(query, conn, params = [end_date, end_date])
            
            df['date'] = pd.to_datetime(df['date'], format = '%Y-%m-%d')
            
            df['days_since_last_game'] = df.apply(lambda x: MiscFeatures.days_since_last_game(games_by_team, x.team_id, x.date, x.season), axis = 1)
            
            df['lat_long'] = df.apply(lambda x: MiscFeatures.location_played(stadiums, x.home_away, x.team_id, x.opponent_id), axis = 1)
            df[['lat', 'long']] = pd.DataFrame(df['lat_long'].tolist(), index = df.index)
            df = df.drop(columns = ['lat_long'])
            
            df['dist_traveled'] = df.apply(lambda x: MiscFeatures.distance_from_last_game(games_by_team, x.team_id, x.date, x.season, x.lat, x.long), axis = 1)
                   
            df['ELO_before'] = df.apply(lambda x: MiscFeatures.previous_elo(games_by_team, x), axis = 1)
        
        games_by_away_team = df.query("home_away == 'away'")[['game_id', 'days_since_last_game', 'dist_traveled', 'ELO_before']]
        games_by_home_team = df.query("home_away == 'home'")[['game_id', 'days_since_last_game', 'dist_traveled', 'ELO_before']]
        
        merged = pd.merge(games_by_away_team, games_by_home_team, on = 'game_id', suffixes = ("_away","_home"))
        merged = merged.drop(columns = ['game_id'])
        
        return index, np.hstack((values, np.array(merged)))

    # Feature Set 6 ------------------------------------------------------------
    def __get_fs6(self):
        df = pd.read_pickle(self.cwd + '\\Saved_Feature_Sets\\FS6\\' + 'fs6.pickle')
        
        x = df.drop(df.columns[[0, 1, 2, 3]], axis = 1)
        y = df[df.columns[3]]

        features = list(x.columns)
        
        return features, np.array(x), np.array(y)
    
    
    def __get_fs6_prediction_set(self, start_date, end_date):
        query1 = """
                    SELECT
                        *
                    FROM games
                    WHERE date = ? and status != 'Ignore'
                """

        query2 = """
                    SELECT 
                        player_id
            
                    FROM boxscores 
            
                    LEFT JOIN games on (games.game_id = boxscores.game_id)
            
                    WHERE status = 'Finished' and team_id = ? and date >= ? and date < ?
            
                    GROUP BY player_id
            
                    ORDER BY min DESC LIMIT 8
                """


        query3 = """
                    SELECT * FROM player_averages WHERE season = 2022 and player_id = ?
                """

        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d')
        start_date = end_date - datetime.timedelta(days = 10)

        col_names = ['game_id', 'date', 'away_team_id', 'home_team_id', 'outcome']
        
        for status in ['away', 'home']:
            for i in range(1, 9):
                for stat in ['fga', 'fgm', '3fga', '3fgm', 'ftm', 'defReb', 'totReb', 'turnovers', 'pFouls']:
                    col_names.append(f'{status}_{stat}_{i}')


        with sqlite3.connect(self.cwd + '\\Database\\' + self.database) as conn:

            cur = conn.cursor()
            games = cur.execute(query1, [end_date.strftime('%Y-%m-%d')]).fetchall()
            
            conn.row_factory = lambda cursor, row: row[0]
            cur = conn.cursor()
            
            df_pred = pd.DataFrame(columns = col_names)
            
            for game_id, season, date, home_team_id, away_team_id, home_team_pts, away_team_pts, status, outcome in games:
                
                away_players = cur.execute(query2, [away_team_id, start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')]).fetchall()
                home_players = cur.execute(query2, [home_team_id, start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')]).fetchall()
                
                players = away_players + home_players
                
                conn.row_factory = None
                x = [game_id, date, away_team_id, home_team_id, outcome]
                for player in players:
                    stats = pd.read_sql(query3, conn, params = [player])
                    
                    stats = stats.drop(columns = ['player_id', 'season', 'AVG(min)'])
                    
                    x = x + [x.values[0] for i, x in stats.iteritems()]
                    
                df_pred.loc[len(df_pred)] = x
        
        index = df_pred[['game_id', 'date', 'away_team_id', 'home_team_id']]
        x = df_pred.drop(columns = ['game_id', 'date', 'away_team_id', 'home_team_id', 'outcome'])
        
        return index, np.array(x)


if __name__ == "__main__":
    
    fsb = FeatureSetBuilder()
    
    #i, x, y = fsb.get('fs5')
    
    i, temp = fsb.get_prediction_set('fs5')
    

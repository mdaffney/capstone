SELECT
	mp.game_id,
	games.date,
	--t2.name as 'away',
	--t1.name as 'home',
	mp.model,
	mp.feature_set,
	mp.prediction_away,
	mp.prediction_home,
	mp.probability_away_win,
	mp.probability_home_win,
	odds.away_team_ml,
	odds.home_team_ml
	
FROM 
model_predictions as mp

LEFT JOIN games on (games.game_id = mp.game_id)

LEFT JOIN teams as t1 on (games.home_team_id = t1.team_id)
LEFT JOIN teams as t2 on (games.away_team_id = t2.team_id)
LEFT JOIN odds on (odds.date = games.date and t1.name = odds.home_team)

WHERE comment is NULL and model = ? and feature_set = ?
and games.date >= ? and games.date <= ?
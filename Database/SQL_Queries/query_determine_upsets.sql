SELECT 
	games.game_id,
	games.date,
	t1.name,
	t2.name,
	CAST(odds.away_team_ml as INT) as 'away_team_ml',
	CAST(odds.home_team_ml as INT) as 'home_team_ml',
	games.outcome,
	CASE
		WHEN CAST(odds.home_team_ml as INT) < 0 and games.outcome = 'win'
			THEN 0
		WHEN CAST(odds.home_team_ml as INT) < 0 and games.outcome = 'loss'
			THEN 1
		WHEN CAST(odds.home_team_ml as INT) > 0 and games.outcome = 'win'
			THEN 1	
		WHEN CAST(odds.home_team_ml as INT) > 0 and games.outcome = 'loss'
			THEN 0
	END as 'upset'

FROM games

LEFT JOIN teams as t1 on (t1.team_id = games.away_team_id)
LEFT JOIN teams as t2 on (t2.team_id = games.home_team_id)

INNER JOIN odds on (odds.date = games.date and odds.away_team = t1.name and odds.home_team = t2.name)

ORDER BY games.date
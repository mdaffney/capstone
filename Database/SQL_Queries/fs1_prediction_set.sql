-- Feature Set 1:
-- Daily Prediction Set

SELECT 
	game_id,
	date,
	away_team_id,
	t1.name as 'away_team_name',
	home_team_id,
	t2.name as 'home_team_name',
	--away team stats
	ROUND(ta.pts, 4) as 'away_pts',
	ROUND(ta.fgm, 4) as 'away_fgm',
	ROUND(ta.fga, 4) as 'away_fga',
	ROUND(ta.fgp, 4) as 'away_fgp',
	ROUND(ta.ftm, 4) as 'away_ftm',
	ROUND(ta.fta, 4) as 'away_fta',
	ROUND(ta.ftp, 4) as 'away_ftp',
	ROUND(ta.'3fgm', 4) as 'away_3fgm',
	ROUND(ta.'3fga', 4) as 'away_3fga',
	ROUND(ta.'3fgp', 4) as 'away_3fgp',
	ROUND(ta.offReb, 4) as 'away_offReb',
	ROUND(ta.defReb, 4) as 'away_defReb',
	ROUND(ta.assists, 4) as 'away_assists',
	ROUND(ta.pFouls, 4) as 'away_pFouls',
	ROUND(ta.steals, 4) as 'away_steals',
	ROUND(ta.turnovers, 4) as 'away_turnovers',
	ROUND(ta.blocks, 4) as 'away_blocks',
	--home team stats
	ROUND(ta2.pts, 4) as 'home_pts',
	ROUND(ta2.fgm, 4) as 'home_fgm',
	ROUND(ta2.fga, 4) as 'home_fga',
	ROUND(ta2.fgp, 4) as 'home_fgp',
	ROUND(ta2.ftm, 4) as 'home_ftm',
	ROUND(ta2.fta, 4) as 'home_fta',
	ROUND(ta2.ftp, 4) as 'home_ftp',
	ROUND(ta2.'3fgm', 4) as 'home_3fgm',
	ROUND(ta2.'3fga', 4) as 'home_3fga',
	ROUND(ta2.'3fgp', 4) as 'home_3fgp',
	ROUND(ta2.offReb, 4) as 'home_offReb',
	ROUND(ta2.defReb, 4) as 'home_defReb',
	ROUND(ta2.assists, 4) as 'home_assists',
	ROUND(ta2.pFouls, 4) as 'home_pFouls',
	ROUND(ta2.steals, 4) as 'home_steals',
	ROUND(ta2.turnovers, 4) as 'home_turnovers',
	ROUND(ta2.blocks, 4) as 'home_blocks'
	
FROM games 

LEFT JOIN team_averages as ta on (ta.team_id = games.away_team_id and ta.home_v_away = 'away' and ta.season = 2022)
LEFT JOIN team_averages as ta2 on (ta2.team_id = games.home_team_id and ta2.home_v_away = 'home' and ta2.season = 2022)
LEFT JOIN teams as t1 on (t1.team_id = games.away_team_id)
LEFT JOIN teams as t2 on (t2.team_id = games.home_team_id)

WHERE games.season = 2022 and status = 'Scheduled' and home_team_pts IS NULL and date = ?
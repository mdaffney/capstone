-- Feature Set 1:
-- Fixed Training Set (all available games in database 
-- prior to 2/19/23 (exclusive))

SELECT 
	games.game_id,
	date,
	away_team_id,
	home_team_id,
	--away team stats
	ROUND(bst.pts, 4) as 'away_pts',
	ROUND(bst.fgm, 4) as 'away_fgm',
	ROUND(bst.fga, 4) as 'away_fga',
	ROUND(bst.fgp, 4) as 'away_fgp',
	ROUND(bst.ftm, 4) as 'away_ftm',
	ROUND(bst.fta, 4) as 'away_fta',
	ROUND(bst.ftp, 4) as 'away_ftp',
	ROUND(bst.'3fgm', 4) as 'away_3fgm',
	ROUND(bst.'3fga', 4) as 'away_3fga',
	ROUND(bst.'3fgp', 4) as 'away_3fgp',
	ROUND(bst.offReb, 4) as 'away_offReb',
	ROUND(bst.defReb, 4) as 'away_defReb',
	ROUND(bst.assists, 4) as 'away_assists',
	ROUND(bst.pFouls, 4) as 'away_pFouls',
	ROUND(bst.steals, 4) as 'away_steals',
	ROUND(bst.turnovers, 4) as 'away_turnovers',
	ROUND(bst.blocks, 4) as 'away_blocks',
	--home team stats
	ROUND(bst2.pts, 4) as 'home_pts',
	ROUND(bst2.fgm, 4) as 'home_fgm',
	ROUND(bst2.fga, 4) as 'home_fga',
	ROUND(bst2.fgp, 4) as 'home_fgp',
	ROUND(bst2.ftm, 4) as 'home_ftm',
	ROUND(bst2.fta, 4) as 'home_fta',
	ROUND(bst2.ftp, 4) as 'home_ftp',
	ROUND(bst2.'3fgm', 4) as 'home_3fgm',
	ROUND(bst2.'3fga', 4) as 'home_3fga',
	ROUND(bst2.'3fgp', 4) as 'home_3fgp',
	ROUND(bst2.offReb, 4) as 'home_offReb',
	ROUND(bst2.defReb, 4) as 'home_defReb',
	ROUND(bst2.assists, 4) as 'home_assists',
	ROUND(bst2.pFouls, 4) as 'home_pFouls',
	ROUND(bst2.steals, 4) as 'home_steals',
	ROUND(bst2.turnovers, 4) as 'home_turnovers',
	ROUND(bst2.blocks, 4) as 'home_blocks',
	outcome
	
FROM games 

INNER JOIN boxscores_team as bst on (bst.game_id = games.game_id and bst.team_id = games.away_team_id and bst.home_v_away = 'away')
INNER JOIN boxscores_team as bst2 on (bst2.game_id = games.game_id and bst2.team_id = games.home_team_id and bst2.home_v_away = 'home')

WHERE status = 'Finished' and date < '2023-02-19'

ORDER BY games.game_id


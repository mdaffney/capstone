SELECT 
	games.season,
	games.date,
	team_id,
	home_v_away,
	AVG(pts),
	AVG(fgm),
	AVG(fga),
	SUM(fgm) / SUM (fga) as 'fgp',
	AVG(ftm),
	AVG(fta),
	SUM(ftm) / SUM (fta) as 'ftp',
	AVG(boxscores_team.'3fgm'),
	AVG(boxscores_team.'3fga'),
	SUM(boxscores_team.'3fgm') / SUM (boxscores_team.'3fga') as 'fg3p',
	AVG(offReb),
	AVG(defReb),
	AVG(assists),
	AVG(pFouls),
	AVG(steals),
	AVG(turnovers),
	AVG(blocks)

FROM boxscores_team
LEFT JOIN games on (games.game_id = boxscores_team.game_id)

WHERE team_id = ? and home_v_away = ? and games.season = ? and date < ?

GROUP BY games.season
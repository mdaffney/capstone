SELECT
	ag.game_id,
	ag.away_team_id as 'team_id',
	ag.season,
	ag.date,
	'away' as 'home_away',
	ag.home_team_id as 'opponent_id',
	away_team_pts as 'pts',
	home_team_pts as 'opponent_pts',
	CASE
		WHEN outcome = 'win' THEN 'loss'
		WHEN outcome = 'loss' THEN 'win'
	END as outcome

FROM games as ag

UNION ALL

SELECT
	hg.game_id,
	hg.home_team_id as 'team_id',
	hg.season,
	hg.date,
	'home' as 'home_away',
	hg.away_team_id as 'opponent_id',
	home_team_pts as 'pts',
	away_team_pts as 'opponent_pts',
	hg.outcome

FROM games as hg

ORDER BY date, game_id, home_away
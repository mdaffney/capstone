# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 09:16:15 2023

@author: kents
"""

import os
import requests
import pandas as pd
import sqlite3




database = 'v1_database.db'

api = r"https://api-nba-v1.p.rapidapi.com"

headers = {
 	"X-RapidAPI-Key": "c767d9abcdmsh1452affa8db93e3p19ffb9jsncbc0c6648549",
 	"X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com"
     }

absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

print(cwd)



def build_games2_table(conn, season):
    conn.row_factory = lambda cursor, row: row[0]
    cur = conn.cursor()
    team_ids = cur.execute("SELECT team_id FROM teams").fetchall()

    endpoint = "/games"

    for team_id in team_ids:

        querystring = {"team": str(team_id), "season": str(season)}
        response = requests.request("GET", api + endpoint, headers = headers, params = querystring).json()
    
        df = pd.json_normalize(response['response'])
        df = games_table_formatting(df, team_id)
    
        df.to_sql('games_team', con = conn, if_exists='append')
    
    # May need if seasons not added in order
    #self.clean_table(conn, 'games')
    
    
def games_table_formatting(df, team_id):
    # Stage 2  --> Regular Season
    df = df.query("league == 'standard' and stage == 2")

    columns_to_keep = [0, 2, 8, 27, 22, 43, 37, 14]      
    df = df.iloc[:, columns_to_keep].copy()

    column_remames = {'id': 'game_id', #0
                      'date.start': 'date', 
                      'teams.home.id': 'home_team_id', #27
                      'teams.visitors.id': 'away_team_id', #22
                      'scores.home.points': 'home_team_pts', #43
                      'scores.visitors.points': 'away_team_pts', #37
                      'status.long': 'status' #14
                      }

    df = df.rename(columns = column_remames)
    del columns_to_keep

    df = df.set_index('game_id')
    df = df.sort_index()

    df['date'] = pd.to_datetime(df['date'], infer_datetime_format = True)
    df['date'] = df['date'].dt.tz_convert('US/Eastern')

    df['date'] = df['date'].dt.strftime('%Y-%m-%d')

    # df['outcome'] = None
    # for index, game in df.iterrows():   
    #     game['outcome'] = 'win' if game['home_team_pts'] > game['away_team_pts'] else 'loss'
    
    df['team_id'] = df.apply(lambda x: team_id, axis=1)
    df['opponent_id'] = df.apply(lambda x: x.away_team_id if team_id == x.home_team_id else x.home_team_id, axis = 1)
    df['home/away'] = df.apply(lambda x: 'home' if x.home_team_id == team_id else 'away', axis=1)
    
    df['outcome'] = df.apply(lambda x: 'win' if (x.home_team_pts > x.away_team_pts and x.home_team_id == team_id) or (x.home_team_pts < x.away_team_pts and x.away_team_id == team_id) else 'loss', axis=1)
    
    df = df[['team_id', 'season', 'date', 'home/away', 'opponent_id', 'outcome']]
    
    return df

def build_players_table(conn, season):
    conn.row_factory = lambda cursor, row: row[0]
    cur = conn.cursor()
    team_ids = cur.execute("SELECT team_id FROM teams").fetchall()

    endpoint = "/players"

    for team_id in team_ids:

        querystring = {"team": str(team_id), "season": str(season)}
        response = requests.request("GET", api + endpoint, headers = headers, params = querystring).json()

        df = pd.json_normalize(response['response'])
        
        temp = df.copy()

        #df = df.iloc[:, 0:17].copy()
        
        df = df[['id', 'firstname', 'lastname', 'college', 'affiliation', 'birth.date',
               'birth.country', 'nba.start', 'nba.pro', 'height.feets',
               'height.inches', 'height.meters', 'weight.pounds', 'weight.kilograms',
               'leagues.standard.jersey', 'leagues.standard.active',
               'leagues.standard.pos']]

        
        column_remames = {'id': 'player_id'}
        df = df.rename(columns = column_remames)
        
        df = df.set_index('player_id')

        df.to_sql('players', con = conn, if_exists='append')
     

conn = sqlite3.connect(cwd + '/Database/' + database)
build_players_table(conn, 2021)
conn.close()
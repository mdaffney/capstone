-- Feature Set 1:
-- Fixed Training Set (all available games in database 
-- prior to 2/19/23 (exclusive))

SELECT 
	games.game_id,
	games.season,
	date,
	away_team_id,
	home_team_id,
	--team stats
	ROUND(bst.pts, 4) as 'pts',
	ROUND(bst.fgm, 4) as 'fgm',
	ROUND(bst.fga, 4) as 'fga',
	ROUND(bst.fgp, 4) as 'fgp',
	ROUND(bst.ftm, 4) as 'ftm',
	ROUND(bst.fta, 4) as 'fta',
	ROUND(bst.ftp, 4) as 'ftp',
	ROUND(bst.'3fgm', 4) as '3fgm',
	ROUND(bst.'3fga', 4) as '3fga',
	ROUND(bst.'3fgp', 4) as '3fgp',
	ROUND(bst.offReb, 4) as 'offReb',
	ROUND(bst.defReb, 4) as 'defReb',
	ROUND(bst.assists, 4) as 'assists',
	ROUND(bst.pFouls, 4) as 'pFouls',
	ROUND(bst.steals, 4) as 'steals',
	ROUND(bst.turnovers, 4) as 'turnovers',
	ROUND(bst.blocks, 4) as 'blocks',
	outcome
	
FROM games 

INNER JOIN boxscores_team as bst on (bst.game_id = games.game_id and bst.team_id = games.home_team_id and bst.home_v_away = 'home')

WHERE status = 'Finished' and date < '2023-02-19'

ORDER BY games.game_id
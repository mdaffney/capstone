# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 10:37:20 2023

@author: kents
"""

import sqlite3
import pandas as pd

conn = sqlite3.connect(r"C:\Users\kents\OneDrive\Desktop\Capstone Local\capstone\Database\v1_database.db")
cur = conn.cursor()

df = pd.read_csv(r"C:\Users\kents\OneDrive\Desktop\stadiums.csv", header=0)

df = df.set_index('stadium_id')

df.to_sql("stadiums", conn, if_exists="append")


conn.close()
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 10:38:08 2023

@author: kents
"""

# Packages
import sys
import os
import pandas as pd
import sqlite3
import pickle
import datetime
from XBNet.training_utils import predict, predict_proba

import warnings
warnings.filterwarnings("ignore")

# File location information

absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

database = 'v1_database.db'

# Our project classes
sys.path.insert(0, cwd + '\\Database')
from FeatureSetBuilder import FeatureSetBuilder

class Model:
    
    def __init__(self, model_name, feature_set_name, date = datetime.date.today().strftime('%Y-%m-%d')):
        
        # Determine relative starting path
        self.absolute_path = os.path.dirname(__file__)

        ap_split = self.absolute_path.split('\\')
        i = ap_split.index('capstone')
        del ap_split[i + 1:]

        self.cwd = '\\'.join(ap_split)
        self.database = 'v1_database.db'

        self.date = date

        # Model related information
        self.valid_models = ['knn', 'logr', 'mlpclassifier', 'lightgbm', 'xgboost', 'adaboost', 'gbt', 'lda', 'randomforest', 'svm', 'naivebayes', 'kmeans', 'TabPFN', 'XBNet']
        
        self.model_name = model_name
        self.feature_set_name = feature_set_name
        self.model = self.get_model(model_name, feature_set_name)

        fsb = FeatureSetBuilder(date = date) 
        self.prediction_index, self.prediction_set = fsb.get_prediction_set(feature_set_name)
        
        
    def predict(self):
        if self.predictionsMade():
            print("\tModel predictions previously made.")
            return
        
        if len(self.prediction_set) == 0:
            return
        
        #TODO scaled prediction set... only works for fs1r currently
        if self.model_name in ['ignore for now']:
            scaler = pickle.load(open(self.cwd + '\\Saved_Models\\%s\\''scaler_%s_%s.sav' % (self.model_name, self.model_name, self.feature_set_name), 'rb'))
            self.prediction_set = scaler.transform(self.prediction_set)
        
        #Predict
        if self.model_name == 'XBNet':
            predictions = predict(self.model, self.prediction_set)
            probabilities = predict_proba(self.model, self.prediction_set).detach().numpy()
        
        else:
            predictions = self.model.predict(self.prediction_set)
            probabilities = self.model.predict_proba(self.prediction_set)
        
        # Write results to database
        self.write_predictions_to_database(predictions, probabilities)
    
    
    def predictionsMade(self):
        query = """SELECT * 
                    FROM model_predictions
                    WHERE date = ? and model = ? and feature_set = ?"""
        
        with sqlite3.connect(self.cwd + '\\Database\\'+ 'v1_database.db') as conn:
            cur = conn.cursor()
            results = cur.execute(query, [self.date, self.model_name, self.feature_set_name]).fetchall()
            
        if len(results) != 0:
            return True
        else:
            return False
    
    def get_model(self, model_name, feature_set_name):
        if model_name in self.valid_models:
            
            filename = self.cwd + '\\Saved_Models\\%s\\%s_%s.sav' % (model_name, model_name, feature_set_name)
            
            if os.path.isfile(filename):
                with open(filename, 'rb') as file:
                    return pickle.load(file)
            else:
                raise FileNotFoundError()
                
        else:
            raise ValueError(f"Not a valid model: {model_name} : {feature_set_name}")
      
            
    def write_predictions_to_database(self, predictions, probablities):
        # Open database connection
        conn = sqlite3.connect(self.cwd + '\\Database\\' + self.database)
        
        # Build initial dataframe
        preds = pd.DataFrame(predictions, columns = ['prediction_home'])
        preds['prediction_away'] = preds.apply(lambda x: 1 if x.prediction_home == 0 else 0, axis = 1)
        
        probs = pd.DataFrame(probablities, columns = ['probability_away_win', 'probability_home_win'])
        
        df = pd.concat([self.prediction_index, preds, probs], axis = 1)
        df['model'] = df.apply(lambda x: self.model_name, axis = 1)
        df['feature_set'] = df.apply(lambda x: self.feature_set_name, axis = 1)
        
        # Format dataframe
        # REQUIRED columns: game_id, model, prediction_home, prediction_away, probablity_home_win, probablity_away_win, comment
        df = df.drop(['away_team_id', 'home_team_id'], axis = 1)
        
        
        df = df[['game_id', 'date', 'model', 'feature_set', 'prediction_home', 'prediction_away', 'probability_home_win', 'probability_away_win']]
        df = df.set_index('game_id')
        
        # Write to database
        df.to_sql('model_predictions', con = conn, if_exists='append')
        
        # Close database connection
        conn.close()

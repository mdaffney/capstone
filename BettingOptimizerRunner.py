# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 09:44:14 2023

@author: kents
"""
# Packages
import os
import sys
import argparse
import sqlite3
from datetime import datetime
from datetime import date
import pandas as pd
from tabulate import tabulate
import random

# File location information

absolute_path = os.path.dirname(__file__)

ap_split = absolute_path.split('\\')
i = ap_split.index('capstone')
del ap_split[i + 1:]

cwd = '\\'.join(ap_split)

database = 'v1_database.db'

# Our project classes
sys.path.insert(0, cwd + '\\Database')
from DatabaseManager import DatabaseManager
from Optimizer import Optimizer

sys.path.insert(1, cwd + '\\Saved_Models\\TabPFN')
from TabPFN import TabPFNClassifierWrapper


def main():
    
    # Arguments for command line interface
    argParser = argparse.ArgumentParser()
    
    argParser.add_argument("-w", "--wager", 
                           type = int, 
                           default = 25, 
                           help = "Betting wager to be optimized (default = $25 USD)")
    
    argParser.add_argument("-s", "--strategy", 
                           choices=['Baseline', 'Moderate', 'Aggressive', 'Conservative'],
                           default = 'Baseline',
                           help = "Betting strategy.")
    
    argParser.add_argument("-m", "--model", 
                           choices = ['knn', 'logr', 'mlpclassifier', 'lightgbm', 'xgboost', 'adaboost', 'gbt', 'lda', 'randomforest', 'svm', 'naivebayes', 'kmeans', 'TabPFN', 'XBNet'], 
                           default = 'knn',
                           help = "Machine learning model used to make outcome predictions.")
    
    argParser.add_argument("-f", "--feature_set", 
                           choices=['fs1', 'fs2', 'fs3', 'fs4', 'fs5'], 
                           default = 'fs1',
                           help = "Feature set used to build model.")
    
    argParser.add_argument("-d", "--date", 
                           default = date.today().strftime('%Y-%m-%d'),
                           type = valid_date,
                           help = "Date of games to be predicted & optimized. Earliest callable date is 2023-03-16. Format = YYYY-MM-DD")
    
    argParser.add_argument("-r", "--random", 
                           action = argparse.BooleanOptionalAction,
                           help = "Randomized parameter selection (overwrites any selections excluding date).")

    args = argParser.parse_args()
    
    if args.random:
        args.wager = random.randint(1, 101)
        args.strategy = random.choice(['Baseline', 'Moderate', 'Aggressive', 'Conservative'])
        args.model = random.choice(['knn', 'logr', 'mlpclassifier', 'lightgbm', 'xgboost', 'adaboost', 'gbt', 'lda', 'randomforest', 'svm', 'naivebayes', 'kmeans', 'TabPFN', 'XBNet'])
        
        if args.model == 'svm':
            args.feature_set = random.choice(['fs1', 'fs2', 'fs3', 'fs4'])
        else:
            args.feature_set = random.choice(['fs1', 'fs2', 'fs3', 'fs4', 'fs5'])
            
    print(args)
    
    # Notify of issues
    if args.wager == 0:
        print("Please enter a wager greater than zero.")
        sys.exit()
    
    if args.model == 'svm' and args.feature_set == 'fs5':
        print("Not a valid model and feature set combination.  Please try again.")
        sys.exit()
        
    if args.model == 'TabPFN':
        print("Note this model may take longer than others to make predictions.")
    
    if args.date < '2023-03-16' or args.date > date.today().strftime('%Y-%m-%d'):
        print("Not a valid data. Dates must be greater than 2023-03-16 and less than or equal to today.  Please try again.")
        sys.exit()
    
    # Check if database is up-to-date (update if not)
    dbm = DatabaseManager(database)
    
    if not dbm.isUpToDate():
        dbm.update_database()
        
    optimizer = Optimizer(args.wager, args.strategy, args.model, args.feature_set, args.date)
    optimizer.optimize()
        
    if args.date < date.today().strftime('%Y-%m-%d'):
        query_results = """
                        SELECT 
                        	date,
                        	away_team,
                        	home_team,
                        	away_team_ml,
                        	home_team_ml,
                        	decision,
                        	ind_wager,
                        	payout,
                            outcome,
                            balance
                        
                        FROM results
                        
                        WHERE date = ? and strategy = ? and model = ? and feature_set = ? and total_wager = ?
                        """
        columns = ['date', 'away_team', 'home_team', 'away_team_ml', 'home_team_ml', 'decision', 'ind_wager', 'payout', 'outcome', 'balance']            
                
    else:
        query_results = """
                        SELECT 
                        	date,
                        	away_team,
                        	home_team,
                        	away_team_ml,
                        	home_team_ml,
                        	decision,
                        	ind_wager,
                        	payout
                        
                        FROM results
                        
                        WHERE date = ? and strategy = ? and model = ? and feature_set = ? and total_wager = ?
                        """
                        
        columns = ['date', 'away_team', 'home_team', 'away_team_ml', 'home_team_ml', 'decision', 'ind_wager', 'payout']
    
    with sqlite3.connect(cwd + '\\Database\\' + database) as conn:
        cur = conn.cursor()
        results = cur.execute(query_results, [args.date, args.strategy, args.model, args.feature_set, args.wager]).fetchall()

    if len(results) == 0:
        print('There are no games today.')
        sys.exit()
        
    
    df = pd.DataFrame(results)
    df.columns = columns
    
    print(tabulate(df, headers='keys', tablefmt='pretty'))


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d").strftime('%Y-%m-%d')
    except ValueError:
        msg = "not a valid date: {0!r}".format(s)
        raise argparse.ArgumentTypeError(msg)


if __name__ == "__main__":
     main()